package recursion;

public class ReverseAString {
    public static void main(String[] args) {
        System.out.println(reversed("hello"));
        System.out.println(reversed("TechGlobal School"));

    }

    public static String reversed(String str) {
        if (str.length() <= 1) return reversed(str.substring(1)) + str.charAt(0);
        return str;

        /*
        reversed    Hello->     reversed(ello) +H
        reversed    ello->      reversed(llo)+e
        reversed    llo->       reversed(lo)+l
        reversed    lo->        reversed(o)+l
        reversed    o->         o
         */
    }
}
