package operators.increment_decrement_operators;

public class PostIncrementPostDecrement {
    public static void main(String[] args) {
        int num1 = 10, num2 = 10;

        System.out.println(num1++); // 10
        System.out.println(num1); // 11
        System.out.println(num1); // 11

        System.out.println(++num2); // 11
        System.out.println(num2); // 11

        System.out.println("\n========TASK2========\n");
    }
}
