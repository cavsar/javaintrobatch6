package conditional_statements;

import java.util.Scanner;

public class Exercise02_MathScore {
    public static void main(String[] args) {
        Scanner input =new Scanner (System.in);

        System.out.println("Hey David! Please enter your math grade?");
        int grade1 = input.nextInt();

        if (grade1>=60){
            System.out.println("Awesome! You have passed the math class!");
        }
        else{
            System.out.println("Sorry! You failed!");


        }
        System.out.println("End of the program");
    }
}
