package conditional_statements;

import java.util.Scanner;

public class Exercise05_CheckAllEven {
    public static void main(String[] args) {


        Scanner input =new Scanner (System.in);

        int a ,b ,c;

        System.out.println( "Please enter 3 numbers ");
        a = input.nextInt();
        b = input.nextInt();
        c = input.nextInt();

        if ( (a % 2==0)&&(b % 2 == 0)  &&(c % 2 == 0) ){
            System.out.println("true");
        }
        else {
            System.out.println("false");
        }
        System.out.println("The end of the program");



    }
}
