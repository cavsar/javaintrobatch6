package projects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class Project06 {
    public static void main(String[] args) {
        findGreatestAndSmallestWithSort();
        findGreatestAndSmallest();
        findSecondGreatestAndSmallestWithSort();
        findSecondGreatestAndSmallest();
        findDuplicatedElementsInAnArray();
        findMostRepeatedElementInAnArray();


    }
    public static void findGreatestAndSmallestWithSort() {
        System.out.println("\n-------TASK-1 - findGreatestAndSmallestWithSort() method---------\n");
        int[] numbers = {10, 7, 7, 10, -3, 10, -3};

        Arrays.sort(numbers);

        System.out.println("Smallest = " + numbers[0]);
        System.out.println("Greatest = " + numbers[numbers.length-1]);

    }

    public static void findGreatestAndSmallest() {
        System.out.println("\n------TASK-2 findGreatestAndSmallest() method-------\n");

        int[] num = {10, 7, 7, 10, -3, 10, -3};
        int min = 0;
        int max = 0;

        for (int number : num) {
            if (number > max) max = number;
            else if (number < min) min = number;

        }
        System.out.println("Smallest = " + min);
        System.out.println("Greatest = " + max);

    }

    public static void findSecondGreatestAndSmallestWithSort() {
        System.out.println("\n------TASK-3 - findSecondGreatestAndSmallestWithSort() method-------\n");

        int[] secondNumbers = {10, 5, 6, 7, 8, 5, 15, 15};
        Arrays.sort(secondNumbers);

        int min=secondNumbers[0];
        int max=secondNumbers[secondNumbers.length-1];
        int SecondSmallest =0;
        int SecondGreatest=0;
        for (int i = 0; i < secondNumbers.length ; i++) {
            if(secondNumbers[i]>min){
        SecondSmallest=secondNumbers[i];
        break;
            }
        }
        for (int i = secondNumbers.length-1; i >0 ; i--) {
            if(secondNumbers[i]<max){
                SecondGreatest=secondNumbers[i];
                break;
            }
            
        }
        System.out.println("Second Smallest = " + SecondSmallest);
        System.out.println("Second Greatest = " + SecondGreatest);
    }

    public static void findSecondGreatestAndSmallest(){
        System.out.println("\n-------TASK-4 - findSecondGreatestAndSmallest() method----\n");

        int[] n ={10, 5, 6, 7, 8, 5, 15, 15};
        int min = Integer.MAX_VALUE, secondMin = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE, secondMax = Integer.MIN_VALUE;
        //[10, 5, 6, 7, 8, 5, 15, 15]
        for(int i  = 0 ; i < n.length; i++){
            if(n[i]< min ){
                min = n[i];
            }
            if(n[i] > min && n[i] < secondMin ){
                secondMin = n[i];
            }
        }
        for(int i  = 0 ; i < n.length; i++){
            if(n[i] < max && n[i] > secondMax){
                secondMax = n[i];
            }
            if(n[i] > max){
                secondMax = max;
                max = n[i];

            }

        }

        System.out.println("The second smallest = " + secondMin);
        System.out.println("The second greatest = " + secondMax);
    }

    public static void findDuplicatedElementsInAnArray() {
        System.out.println("\n-------TASK-5 - findDuplicatedElementsInAnArray() method ---\n");
        String[] words = {"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};
        for (int i = 0; i < words.length - 1; i++) {
            for (int j = i + 1; j < words.length; j++) {
                if (words[i].equals(words[j])) {
                    System.out.println(words[i]);

                }
            }

        }}
public static void findMostRepeatedElementInAnArray() {
        System.out.println("\n------TASK-6 findMostRepeatedElementInAnArray() method ----\n");
        String [] w={"pen", "eraser", "pencil", "pen", "123", "abc", "pen", "eraser"};

    int maxCount = 0;
    String mostRepeated = "";

    // Loop through each element of the array
    for (int i = 0; i < w.length; i++) {
        int count = 1; // Start with a count of 1 for the current element
        for (int j = i + 1; j < w.length; j++) {
            // If the current element is matched to the next element, the count is increased.
            if (w[i].equals(w[j])) {
                count++;
            }
        }

        // If the count for the current element is greater than the max count,
        // update the max count and most repeated element
        if (count > maxCount) {
            maxCount = count;
            mostRepeated = w[i];
        }
    }

    // Print the most repeated element
    System.out.println(mostRepeated);
}
}








