package projects;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Project05 {
    public static void main(String[] args) {
         /*
    TASK-1
    Write a program that asks user to enter a sentence as a String, and count
    how many words that sentence has, and print it with given below message.
    NOTE: Write a program that handles any String
    NOTE: First check if the given sentence has 2 words at least. If not, then just
    print “This sentence does not have multiple words”.
     */
        System.out.println("\n========TASK1========\n");

        String sent1 = ScannerHelper.getString();

        sent1=sent1.trim();
        if (sent1.contains(" "))
            System.out.println("This sentence does not have multiple words");
        else {
            int counter =0;
            while (sent1.contains(" "))
                sent1=sent1.substring(sent1.indexOf(" ")+1).trim(); counter++;
            System.out.println("This sentence has "+counter+ " words.");}
/*
    TASK-2
    Write a program that generates 2 random numbers between 0 and 25 (0 and
    25 are included), Then print all numbers between 2 random numbers that
    cannot be divided by 5 in ascending order.

 */


        System.out.println("\n========TASK2========\n");
        int randomNum1 = (int) (Math.random() * 26);
        int randomNum2 = (int) (Math.random() * 26);
        System.out.println(randomNum1);
        System.out.println(randomNum2);

        String str1 = "";

        for (int i = Math.min(randomNum1,randomNum2); i <= Math.max(randomNum1,randomNum2); i++) {
            if(i % 5 !=0) str1 += i + " - ";
        }
        System.out.println(str1.substring(0,str1.length() - 3));
 /*
    TASK-3
    Write a program that asks user to enter a sentence as a String and count
    how many a or A letters that sentence has and print it with given below
    message.
    NOTE: Write a program that handles any String
    NOTE: First check if the given sentence has 1 character at least. If not, then
    just print “This sentence does not have any characters”.
     */
        System.out.println("\n========TASK3========\n");
        String str2 = ScannerHelper.getString();
        int counter1 = 0;

        if (str2.length() == 0) {
            System.out.println("This sentence does not have any characters.");
        }
        else {
            for (int i = 0; i < str2.length(); i++) {
                if (str2.charAt(i) == 'a' || str2.charAt(i) == 'A') counter1++;
            }
            System.out.println("This sentence has " + counter1 + " a or A letters.");
        }
         /*
    TASK-4
    Write a program that asks user to enter a word, and check if it is palindrome
    or not.
    Palindrome: It is a word that is read the same backward as forward
    •EX/ kayak, civic, madam
    NOTE: Write a program that handles any String
    NOTE: First check if the given String has at least 1 character, if the String
    does not have at least one character, then print message “This word does
    not have 1 or more characters”
     */

        System.out.println("\n========TASK4========\n");
        String ans10 =ScannerHelper.getString();
        boolean isPalindrome = true;

        if (ans10.length() < 1) {
            System.out.println("This word does not have 1 or more characters.");
        } else {
            for (int i = 0, j = ans10.length() - 1; i <= ans10.length() / 2; i++, j--) {
                if (ans10.charAt(i) == ans10.charAt(j)) {
                    isPalindrome = false;
                    break;
                }
            }
            if (isPalindrome) System.out.println("This word is a palindrome");
            else System.out.println("This word is not a palindrome");
        }

/*
    TASK-5
    Write a program that prints the shape below.
                        *
                      * * *
                    * * * * *
                  * * * * * * *
                * * * * * * * * *
              * * * * * * * * * * *
            * * * * * * * * * * * * *
          * * * * * * * * * * * * * * *
        * * * * * * * * * * * * * * * * *
     */

        System.out.println("\n========TASK5========\n");

        for (int i = 0; i <= 9; i++) {
            for (int j = i; j <= 9; j++) {
                System.out.print("  ");
            }
            for (int k = 0; k < (2 * i) + 1; k++) {
                System.out.print("*");
                System.out.print(" ");
            }
            System.out.println(" ");


        }


    }

}











