package projects;

import java.time.Year;
import java.util.Scanner;

public class Project02 {
    public static void main(String[] args) {

        System.out.println("\n=========TASK1==========\n");
        Scanner inputReader = new Scanner (System.in);

       System.out.println("Please enter 3 numbers");
        int number1 = inputReader.nextInt();
        int number2 = inputReader.nextInt();
        int number3 = inputReader.nextInt();

        System.out.println("The product of the numbers entered is " + number1 * number2 *number3 );

        System.out.println("\n=========TASK2==========\n");
        String fname, lname;
        int yearOfBirth ;
        int currentYear = Year.now().getValue();
        inputReader.nextLine();

        System.out.println("What is your first name? ");
        fname = inputReader.nextLine();

        System.out.println("What is your last name? ");
        lname = inputReader.nextLine();

        System.out.println("What is your year of birth ");
        yearOfBirth = inputReader.nextInt();

        System.out.println("What is the current year? ");
        currentYear= inputReader.nextInt();

        System.out.println(" "+ fname +" " + lname + " 's age is = " +(currentYear - yearOfBirth) +"." );
        inputReader.nextLine();
        System.out.println("\n=========TASK3==========\n");

        String fullName;
        double weight;
        double converter = 2.205;

        System.out.println("What is your full name? ");
        fullName = inputReader.nextLine();

        System.out.println("What is your weight? ");
        weight = inputReader.nextDouble();
        inputReader.nextLine();

        System.out.println(fullName+ "'s weight is = " +weight * converter + " lbs. ");

        System.out.println("\n=========TASK4==========\n");
        String fName1, fName2, fName3;
        int age1, age2, age3;

        System.out.println("What is your full name? ");
        fName1 = inputReader.nextLine();

        System.out.println("What is your age? ");
        age1 = inputReader.nextInt();

        System.out.println("What is your full name? ");
        inputReader.nextLine();
        fName2 = inputReader.nextLine();

        System.out.println("What is your age? ");
        age2 = inputReader.nextInt();
        System.out.println("What is your full name? ");
        inputReader.nextLine();
        fName3= inputReader.nextLine();
        System.out.println("What is your age? ");
        age3 = inputReader.nextInt();
        inputReader.nextLine();
        System.out.println(fName1 +"'s age is "+ age1 + ".");
        System.out.println(fName2 +"'s age is "+ age2 + ".");
        System.out.println(fName3 +"'s age is "+ age3 + ".");
        System.out.println("The average age is " + (age1+age2+age3)/3);
        System.out.println("The eldest age is "+Math.max(Math.max(age1,age2),age3));
        System.out.println("The youngest age is "+Math.min(Math.min(age1,age2),age3));


}}
