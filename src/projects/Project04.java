package projects;

import utilities.ScannerHelper;

public class Project04 {
    public static void main(String[] args) {
        System.out.println("\n============= TASK1 =============\n");
        String s1 = ScannerHelper.getString();

        int len = s1.length();
        if (len < 8) System.out.println("This String does not have 8 characters");
        else System.out.println(s1.substring(len - 4) + s1.substring(4, len - 4) + s1.substring(0, 4));

        System.out.println("\n============= TASK2 =============\n");
        String s = ScannerHelper.getString();
        int empty2 = s.lastIndexOf(" ");
        int empty1 = s.indexOf(" ");
        if (!s.contains(" ")) System.out.println("This sentence does not have 2 or more words to swap");
        else
            System.out.println(s.substring(empty2).trim() + s.substring(empty1, empty2) + " " + s.substring(0, empty1));

        System.out.println("\n============= TASK3 =============\n");

        String str1 = "These books are so stupid";
        String str2 = "I like idiot behaviors";
        String str3 = "I had some stupid t-shirts in the past and also some idiot look shoes";

        System.out.println(str1.replace("stupid", "nice"));
        System.out.println(str2.replace("idiot", "nice"));
        System.out.println(str3.replace(("stupid"), "nice").replace("idiot", "nice"));

        System.out.println("\n============= TASK4 =============\n");
        String name = ScannerHelper.getFirstName();
        int lengthOfName = name.length();
        if (lengthOfName < 2) System.out.println("Invalid input!!!");
        else {
            if (lengthOfName % 2 == 0) System.out.println(name.substring(lengthOfName / 2 - 1, (lengthOfName / 2) + 1));
            else System.out.println(name.charAt(lengthOfName / 2));

            System.out.println("\n============= TASK5 =============\n");
            String country = ScannerHelper.getCountry();
            int lengthOfCountry = country.length();
            if (lengthOfCountry <= 5) System.out.println("Invalid input!!");
            else System.out.println(country.substring(2, lengthOfCountry - 2));


            System.out.println("\n============= TASK6 =============\n");
            String address = ScannerHelper.getAddress();
            System.out.println(address.replace("a", "*").replace("e", "#")
                    .replace("i", "+").replace("u", "$").replace("o", "@")
                    .replace("A", "*").replace("E", "#")
                    .replace("I", "+").replace("U", "$").replace("O", "@"));


            System.out.println("\n============= TASK7 =============\n");
            int numberOfWord = 0;
            String sent1 = ScannerHelper.getString();
            if (sent1.indexOf(" ") == sent1.lastIndexOf(" "))
                System.out.println("This sentence does not have multiple words");
            else System.out.println("This sentence has n words.");

        }

    }}




