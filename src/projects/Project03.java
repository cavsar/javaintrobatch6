package projects;

import conditional_statements.IfElseSyntax;

import java.util.Scanner;

public class Project03 {
    public static void main(String[] args) {
        System.out.println("\n============= TASK1 =============\n");

        String s1 = "24", s2 ="5";
        int num1 = Integer.parseInt(s1);
        int num2 = Integer.parseInt(s2);
        System.out.println("The sum of "+ num1 +" and "+  num2 + " = " + (num1+num2) );
        System.out.println("The subtraction of "+ num1 +" and "+  num2 + " = " + (num1-num2) );
        System.out.println("The division of "+ num1 +" and "+  num2 + " = " + ((double) num1/num2) );
        System.out.println("The multiplication of "+ num1 +" and "+  num2 + " = " + (num1*num2) );

        System.out.println("\n============= TASK2 =============\n");

        int number = (int)(Math.random()*35 )+ 1;
        System.out.println(number);

        if(number == 2 || number == 3 || number == 5 || number % 2 != 0 || number % 3 != 0 || number % 5 != 0 || number % 11 != 0 ) System.out.println( number + " IS A PRIME NUMBER");
        else if (number ==1) System.out.println( number + "IS NOT A PRIME NUMBER");
        else System.out.println( number + " IS NOT A PRIME NUMBER");

        System.out.println("\n============= TASK3 =============\n");

        int a = (int)(Math.random()*50) +1;
        int b = (int)(Math.random()*50) +1;
        int c = (int)(Math.random()*50) +1;

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);

        if(a<b  && b<c) {
            System.out.println(a);
            System.out.println(b);
            System.out.println(c);}
        else if (b<c && c<a) {
            System.out.println(b);
            System.out.println(c);
            System.out.println(a);}
        else if (c<b && b<a) {
            System.out.println(c);
            System.out.println(b);
            System.out.println(a);}
        else if (a<c && c<b) {
            System.out.println(a);
            System.out.println(c);
            System.out.println(b);}
        else if (c<a && a<b) {
            System.out.println(c);
            System.out.println(a);
            System.out.println(b);}
        else if (b<a && a<c) {
            System.out.println(b);
            System.out.println(a);
            System.out.println(c);}
        System.out.println("\n============TASK4==============\n");

        char letter = 'a';

        if((letter >= 65 && letter <= 90) || (letter >= 97 && letter <= 122)) {
            System.out.println("Character is a letter");
            if ((letter >= 97 && letter <= 122))System.out.println("The letter is lowercase");
            else System.out.println("The letter is uppercase");}
        else System.out.println("Invalid character detected!!!");
        System.out.println("\n============= TASK5 =============\n");

        char Char = 'b';

        if((Char >= 65 && Char <= 90) || (Char >= 97 && Char <= 122)) {
            System.out.println("Character is a letter");
            if ( Char == 65 || Char ==97|| Char == 69 || Char == 101 || Char ==73 || Char ==105 || Char == 79 || Char ==111)
                System.out.println("The letter is vowel");
            else {
                System.out.println("The letter is consonant");}
        }
            else{ System.out.println("Invalid character detected!!!");}


        System.out.println("\n============= TASK6 =============\n");

        char character ='#';
        if((character>=33 && character<=47) ||(character >= 58 && character <= 64)){System.out.println("Special character is = " + character + ".");}
        else{ System.out.println("Invalid character detected" +".");}

          System.out.println("\n============= TASK7 =============\n");
        char x = '@';

        if((x >= 65 && x <= 90) || (x >= 97 && x <= 122)){ System.out.println("Character is a letter");}
        else if(x >= 48 && x <= 57) {System.out.println("Character is a digit");}
        else if((x>=33 && x<=47) ||(x >= 58 && x <= 64)){System.out.println("Character is a special character");}

        }}
