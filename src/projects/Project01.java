package projects;

public class Project01 {
    public static void main(String[] args) {

        System.out.println("\n==========TASK1===========\n");
        String name="Cihan";

        System.out.println("My name is" +" " +name);

        System.out.println("\n==========TASK2===========\n");
        char nameCharacter1= 'C';
        char nameCharacter2= 'i';
        char nameCharacter3= 'h';
        char nameCharacter4= 'a';
        char nameCharacter5= 'n';

        System.out.println("Name Letter 1 is"+" "+nameCharacter1);
        System.out.println("Name Letter 2 is"+" "+nameCharacter2);
        System.out.println("Name Letter 3 is"+" "+nameCharacter3);
        System.out.println("Name Letter 4 is"+" "+nameCharacter4);
        System.out.println("Name Letter 5 is"+" "+nameCharacter5);

        System.out.println("\n==========TASK3===========\n");

        String myFavMovie="Buyuk Adam Kucuk Ask";
        System.out.println("My favorite movie is "+myFavMovie);

        String myFavSong="Perfect";
        System.out.println("My favorite song is "+myFavSong);

        String myFavCity="Diyarbakir";
        System.out.println("My favorite city is "+myFavCity);

        String myFavActivity="dancing";
        System.out.println("My favorite activity is "+myFavActivity);

        String myFavSnack="chocolate";
        System.out.println("My favorite snack is "+myFavSnack);

        System.out.println("\n==========TASK4===========\n");

        int myFavNumber;
        myFavNumber = 21;
        System.out.println("My favorite number is "+ myFavNumber);

        byte numberOfStatesIVisited;
        numberOfStatesIVisited=10;
        System.out.println("The number of states that I visited is "+ numberOfStatesIVisited);

        short numberOfCountriesIVisited;
        numberOfCountriesIVisited=5;
        System.out.println("The number of Countries that I visited is "+ numberOfCountriesIVisited);

        System.out.println("\n==========TASK5===========\n");

        String beingAtSchool;
        boolean amIAtSchoolToday =true;
         if (amIAtSchoolToday) {
             beingAtSchool = "I am at school today";
         }else {beingAtSchool ="I am joining online today";}

        System.out.println(beingAtSchool);

        System.out.println("\n==========TASK6===========\n");

        String Weather;
        boolean isWeatherNiceToday =true ;
        if (isWeatherNiceToday) {
            Weather = "Weather is nice today ";
        }else {Weather ="it is below or equal to 60F today";}

        System.out.println(Weather);
    }
}
