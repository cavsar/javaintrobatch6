package projects;

import java.util.Arrays;

public class Project08 {
    public static void main(String[] args) {
        System.out.println("******Task1*****");
        System.out.println(findClosestDistance(new int[]{4}));
        System.out.println(findClosestDistance(new int[]{4, 8, 7, 15}));
        System.out.println(findClosestDistance(new int[]{10, -5, 20, 50, 100}));

        System.out.println("******Task2*****");
        System.out.println(findSingleNumber(new int[]{2}));
        System.out.println(findSingleNumber(new int[]{5, 3, -1, 3, 5, 7, -1}));

        System.out.println("******Task3*****");
        System.out.println(findFirstUniqueCharacter("Hello"));
        System.out.println(findFirstUniqueCharacter("abc abc d"));
        System.out.println("******Task4*****");
        System.out.println(findingMissingNumber(new int[]{2, 4}));
        System.out.println(findingMissingNumber(new int[]{2, 3, 1, 5}));
        System.out.println(findingMissingNumber(new int[]{4, 7, 8, 6}));
    }

    public static int findClosestDistance(int[] arr) {


        if (arr.length < 2) {
            return -1;
        }
        Arrays.sort(arr);
        int diffMin = Integer.MAX_VALUE;
        for (int i = 1; i < arr.length; i++) {

            int dif = arr[i] - arr[i - 1];
            if (dif < diffMin) {
                diffMin = dif;
            }
        }
        return diffMin;
    }


    public static int findSingleNumber(int[] arr) {

        Arrays.sort(arr);
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] != arr[i]) {
                return arr[i];
            }
        }
        return arr[arr.length - 1];
    }


    public static char findFirstUniqueCharacter(String str) {

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            if (str.indexOf(c) == str.lastIndexOf(c)) {
                return c;
            }
        }
        return 0;
    }

    public static int findingMissingNumber(int[] arr) {
        Arrays.sort(arr);

        for (int i = 0; i < arr.length; i++) {
            if (arr[i + 1] - arr[i] != 1) {
                return arr[i] + 1;
            }
        }
        return 0;
    }
}
