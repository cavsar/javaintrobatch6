package casting;

public class Exercise01 {
    public static void main(String[] args) {
        int num1 = 5;
        int num2 =2;

        System.out.println((double)num1/ num2);
        System.out.println(num1/(double)num2);
        System.out.println((double)num1/(double)num2);

        System.out.println((double)(num1/num2)); // it won't be double
    }
}
