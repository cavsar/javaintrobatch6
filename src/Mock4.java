import java.util.HashMap;
import java.util.Map;

public class Mock4 {


    /*
    Requirement:
-Student is responsible to create a main method and test their method.

-Write a method that takes an array of String and returns the count of each unique element in the array as a Map

Test Data:
[“Apple”, “Apple”, “Orange”, “Apple”, “Kiwi”]

Expected:
{Apple=3, Orange=1, Kiwi=1}
     */

    public static Map<String,Integer> counter(String[]arr) {
        Map<String, Integer> count = new HashMap<>();
        for (String fruit : arr) {
            if (count.containsKey(fruit)) {
                count.put(fruit, count.get(fruit + 1));
            } else {
                count.put(fruit, 1);
            }
        }return count;
    }




}
