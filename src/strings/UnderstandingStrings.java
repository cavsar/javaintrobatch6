package strings;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class UnderstandingStrings {
    public static void main(String[] args) {
        String s1; //declaration of s1 as a string

        s1="techGlobal School"; //initializing s1 as TechGlobal School

        String s2="is the best"; // declaration and initializing s2


        System.out.println(s1);
        System.out.println(s2);

        System.out.println("--------CONCAT USING+-------\n");
        String s3= s1+" "+ s2; //  concationation using plus sign

        System.out.println(s3); // techGlobal School is the best

        System.out.println("---------CONCAT USING METHOD------\n");
        String s4= s1.concat(" ").concat(s2);
        System.out.println(s4);

        String wordPart1 ="le";
        String wordPart2 ="ar";
        String wordPart3 ="ning";
        String wordPart4= wordPart1+wordPart2+wordPart3;
        System.out.println(wordPart4);
    }
}
