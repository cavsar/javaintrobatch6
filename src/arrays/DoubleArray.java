package arrays;

import java.util.Arrays;

public class DoubleArray {
    public static void main(String[] args) {
        System.out.println("\n------Task1-------\n");
        double[] numbers = {5.5, 6, 10.3, 25};
        System.out.println(Arrays.toString(numbers));
        System.out.println("The length is " + numbers.length);
        for (double element : numbers) {
            System.out.println(element);
        }



    }
}
