package arrays;

import java.util.Arrays;

public class UnderstandingArrays {
    public static void main(String[] args) {
        String[] cities = {"Chicago", "Miami", "Toronto"};

        // The number elements in the array
        int sizeOfTheArray = cities.length;
        System.out.println(sizeOfTheArray);
        System.out.println(cities.length);// second way

        // Get particular element from the array
        System.out.println(cities[1]);
        System.out.println(cities[0]);//Chicago
        System.out.println(cities[2]);//Toronto

        // ArrayIndexOutOfBoundsException
        //System.out.println(cities[-2]);

        // HOW TO PRINT THE ARRAY->[Chicago,Miami,Toronto]
        //1. Convert your array to a String
        //2.Print it

        System.out.println(Arrays.toString(cities));

        System.out.println("\n=====for loop======\n");
        //HOW TO LOOP AN ARRAY
        for (int i = 0; i < cities.length; i++) {
            System.out.println(cities[i]);

            System.out.println("\n=====for each loop - enchanced loop======\n");
            for(String element: cities){
                System.out.println(element);
            }


        }
    }
}
