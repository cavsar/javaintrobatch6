package arrays;

import java.util.Arrays;

public class Exercise03_SearchInArray {
    public static void main(String[] args) {
        String [] objects ={"Remote", "Mouse", "Mouse", "Keyboard", "iPad"};
        boolean containsMouse = false;
        for (String object : objects) {
            if(object.contains ("Mouse")){
                containsMouse = true;
                break;
            }

        }
        System.out.println(containsMouse);

        Arrays.sort(objects);
        System.out.println(Arrays.binarySearch(objects, "Mouse") >= 0);


    }
}
