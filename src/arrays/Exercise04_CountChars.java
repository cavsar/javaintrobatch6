package arrays;

import utilities.ScannerHelper;

public class Exercise04_CountChars {
    public static void main(String[] args) {
        String s = ScannerHelper.getString().toLowerCase();

        System.out.println("\n======1st way=====\n");
        int counter = 0;

        for (int i = 0; i < s.length(); i++) {
            if (Character.isLetter(s.charAt(i))) counter++;
        }
        System.out.println(counter);

        System.out.println("\n======2nd way=====\n");
        s = ScannerHelper.getString();
        counter=0;
        char[] chars = s.toCharArray();
        for (char a : chars) {
            if(Character.isLetter(a)) counter++;
            
        }
        System.out.println(counter);

        System.out.println("\n-------3rd way------\n");  // toCharArray()

        int letters = 0;

        for (char c : ScannerHelper.getString().toCharArray()) {
            if(Character.isLetter(c)) letters++;
        }

        System.out.println(letters);
    }
}
