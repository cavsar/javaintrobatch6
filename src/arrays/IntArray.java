package arrays;

import java.util.Arrays;

public class IntArray {
    public static void main(String[] args) {

        //Create an int array that will store 6 numbers
        int[] numbers = new int[6];
        System.out.println(Arrays.toString(numbers)); //[0,0,0,0,0]
        System.out.println(numbers);// it gives the location so that you will get [I@1b6d3586
        System.out.println(numbers[3]); //0


        //HOW TO ASSIGN A VALUE TO AN EXISTING VALUE
        numbers[0] = 5;
        numbers[2] = 15;
        numbers[4] = 25;
        //numbers[7] = 45; //outofbound

        System.out.println(Arrays.toString(numbers));//[5, 0, 15, 0, 25, 0]
        numbers[0] = 7;
        System.out.println(Arrays.toString(numbers));

        // Print each element with for loop
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);

            // Print each element with for each loop-enchanced for loop
            for (int number : numbers) {
                System.out.println(number);
            }
           //shortcut
            for (int number : numbers) {
                System.out.println(number);

            }
        }

    }
}
