package arrays.practice01;

import javax.naming.PartialResultException;

public class Exercise04 {
    public static void main(String[] args) {
        containsApple(new String[] {"pineapple"});
    }
        /*Write a program to find if String array contains “apple” as an element, ignore cases.
Test data 1:
String[] list = {“banana”, “orange”, “Apple”};
Expected output:
true
Test data 2:
String[] list = {“pineapple”, “banana”, “orange”,
“grapes”};
Expected output:
false
NOTE: Make your code dynamic that works for any

         */

        public static void containsApple(String[] arr) {
            boolean containApple = false;
            for (String s : arr) {
                if(s.equalsIgnoreCase("apple")){
                    containApple = true;
                    break;
                }
            }
            System.out.println(containApple);

    }
}
