package mock;

import java.util.Scanner;

public class HmwTask3 {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your favorite country");
        String favCountry = input.nextLine();
        if(favCountry.toLowerCase().contains("a") &&favCountry.toLowerCase().contains("i") ) System.out.println("A and i are there");
        else if(favCountry.toLowerCase().contains("a")) System.out.println("A is there");
        else if(favCountry.toLowerCase().contains("i")) System.out.println("I is there");
        else System.out.println("A and i are not there");
    }
}
