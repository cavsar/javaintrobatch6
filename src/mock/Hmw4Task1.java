package mock;

import java.util.Scanner;

public class Hmw4Task1 {
    public static <input> void main (String[]args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your name");
        String name=input.nextLine();
        int len= name.length();
        System.out.println(len);
        System.out.println(name.charAt(0));
        System.out.println(name.substring(0,3));
        System.out.println(name.substring(len-3));
        if(name.toLowerCase().startsWith("a")) System.out.println("You are in the club!");
        else System.out.println("Sorry, you are not in the club");

    }
}
