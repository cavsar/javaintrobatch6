package mock;

import java.util.Scanner;

public class Hmw4Task2 {
    public static void main (String[] args){

        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your full address");
        String fullAdr= input.nextLine();
        if(fullAdr.toLowerCase().contains("chicago")) System.out.println ("You are in the club");
        else if(fullAdr.toLowerCase().contains("des plaines")) System.out.println ("You are welcome to join to the club");
        else System.out.println("Sorry, you will never be in the club");
    }
}
