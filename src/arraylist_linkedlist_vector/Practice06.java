package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Practice06 {
    public static void main(String[] args) {
        System.out.println("\n-------Task1---------\n");
        System.out.println(Arrays.toString(double1(new int[]{3, 2, 5})));

        System.out.println("\n-------Task2---------\n");
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(5, 7, 2, 2, 10, 10));
        System.out.println(secondMax(list));

        System.out.println("\n-------Task3---------\n");
        ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(3, 2, 3, 2, 7, 7));
        System.out.println(secondMin(list1));

        System.out.println("\n-------Task4---------\n");
        System.out.println(removeEmpty(new ArrayList<>(Arrays.asList("Tech", "Global", "", "", "School"))));
        System.out.println(removeEmpty(new ArrayList<>(Arrays.asList("", ""))));

        System.out.println("\n-------Task5---------\n");
        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(200, 2, -123, 2, 7, 7));
        System.out.println(remove3OrMore(list2));

        System.out.println("\n-------Task6---------\n");
        System.out.println(uniquesWords("Tech Global"));


    }


    /*Write a method called as
    double
     to double each element
    in an int array and return it back.
    NOTE: The return type is an array.
    Test data 1:
    {3, 2, 5, 7, 0}
    Expected output 1:
    [6, 4, 10, 14, 0]
    Test data 2:
    [-2, 0, 3, 10, 100]
    Expected output 2:
    [-4, 0, 6, 20, 200]
    NOTE: Make your code dynamic that works for any given
    array.

     */
    public static int[] double1(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i] * 2;

        }
        return arr;
/*
Write a method called as
secondMax
 to find and return
the second max number in an ArrayList
Test data 1:
{2, 3, 7, 1, 1, 7, 1}
Expected output 1:
3
Test data 2:
[5, 7, 2, 2, 10, 10]
Expected output 2:
7
NOTE: Make your code dynamic that works for any
given ArrayList
 */
    }

    public static int secondMax(ArrayList<Integer> list) {

        Collections.sort(list);//1, 1, 1, 2, 3, 7, 7

        for (int i = list.size() - 2; i >= 0; i--) {
            if (list.get(i) < list.get(list.size() - 1)) return list.get(i);
        }
        return 0;


    /*
    int max = Integer.MIN_VALUE;
    int secondMax = Integer.MIN_VALUE;

    //WAY1
    for (Integer n : list) {
        max = Math.max(max, n);
    }
    for (Integer n : list) {
        if(n > secondMax && n < max) secondMax = n;
    }

    //WAY2
    for (Integer n : list) {
        if(n > max) {
            secondMax = max;
            max = n;
        }
    }

     */


    }

    public static int secondMin(ArrayList<Integer> list) {

        Collections.sort(list);//1, 1, 1, 2, 3, 7, 7

        for (int i = 1; i <= list.size(); i++) {
            if (list.get(i) > list.get(0)) return list.get(i);
        }
        return 0;
    }

    /*Write a method called as
    removeEmpty
     to find and
    remove all the elements in an ArrayList that are empty or
    null.
    Then, return the modified ArrayList back.
    Test data 1:
    ["Tech", "Global", "", null, "", "School"]
    Expected output 1:
    ["Tech", "Global", "School"]
    Test data 2:
    ["", "", ""]
    Expected output 2:
    []
    NOTE: Make your code dynamic that works for any given
    ArrayList

    }

     */
    public static ArrayList<String> removeEmpty(ArrayList<String> list) {
        list.removeIf(element -> element == null || element.isEmpty());
        return list;
    }

    /*Write a method called as
remove3orMore
 to find and remove all
the elements in an ArrayList that are more than 2 digits.
Then, return the modified ArrayList back.
NOTE: - sign should not counted as a digit when it is negative
number.
Test data 1:
[200, 5, 100, 99, 101, 75]
Expected output 1:
[5, 99, 75]
Test data 2:
[-12, -123, -5, 1000, 500, 0]
Expected output 2:
[-12, -5, 0]
NOTE: Make your code dynamic that works for any given ArrayList

     */
    public static ArrayList<Integer> remove3OrMore(ArrayList<Integer> num) {
        num.removeIf(element -> Math.abs(element) / 100 >= 1);
        return num;
    }
/*
Write a method called as
uniquesWords
 to find and return all the
unique words in a String.
NOTE: The return type is an ArrayList.
NOTE: Assume that you will not be given extra spaces.
Test data 1:
"TechGlobal School”
Expected output 1:
["TechGlobal", "School"]
Test data 2:
"Star Light Star Bright"
Expected output 2:
["Star", "Light", "Bright"]
NOTE: Make your code dynamic that works for any given String



 */
    public static ArrayList<String> uniquesWords(String str) {
        ArrayList<String>strAsList =new ArrayList<>();
        for (String s : str.split(" ")) {
            if(!strAsList.contains(s)) strAsList.add(s);
        }
        return strAsList;
        }


    }






