package utilities;

import java.sql.SQLOutput;
import java.util.Scanner;

public class ScannerHelper {
    static Scanner input = new Scanner(System.in);

    // Write a method that ask and return a first name from user
    public static String getFirstName() {
        System.out.println("Please enter a first name:");
        return input.nextLine();
    }


    // Write a method that ask and return a last name from user - returns String
    // method name should be getLastName
    // it should be static

    public static String getLastName() {
        System.out.println("Please enter a last name:");
        return input.nextLine();
    }

    // Write a method that ask and return an age from user - int
// method name should be getAge
// it should be static
    public static int getAge() {
        System.out.println("Please enter an age:");
        return input.nextInt();
    }

    public static int getNumber() {
        System.out.println("Please enter a number:");
        int number = input.nextInt();
        input.nextLine();

        return number;
    }

    public static String getString() {
        System.out.println("Please enter a String");
        String str = input.nextLine();

        return str;
    }

    public static String getFavoriteBook() {
        System.out.println("Please enter your favorite book");
        String favBook = input.nextLine();
        return favBook;
    }

    public static String getFavoriteQuote() {
        System.out.println("Please enter your favorite quote");
        String favQuote = input.nextLine();
        return favQuote;
    }

    public static String getCountry() {
        System.out.println("Please enter your favorite country");
        String favCountry = input.nextLine();
        return favCountry;
    }

    public static String getAddress() {
        System.out.println("Please enter your address");
        String myAddress = input.nextLine();
        return myAddress;


    }
    public static String getUserName() {
        System.out.println("Please enter an username");
        String userName = input.nextLine();
        return userName;

}}