package homeworks;

public class Homework14 {
    public static void main(String[] args) {
        System.out.println("====Task1====");
        fizzBuzz1(3);
        fizzBuzz1(5);
        fizzBuzz1(18);

        System.out.println("====Task2====");
        System.out.println(fizzBuzz2(0));
        System.out.println(fizzBuzz2(1));
        System.out.println(fizzBuzz2(3));
        System.out.println(fizzBuzz2(5));
        System.out.println(fizzBuzz2(15));

        System.out.println("====Task3====");
        System.out.println(findSumNumbers("abc$"));
        System.out.println(findSumNumbers("a1b4c  6#"));
        System.out.println(findSumNumbers("ab110c045d"));
        System.out.println(findSumNumbers("525"));

        System.out.println("====Task4====");
        System.out.println(findBiggestNumber("abc$"));
        System.out.println(findBiggestNumber("a1b4c  6#"));
        System.out.println(findBiggestNumber("ab110c045d"));
        System.out.println(findBiggestNumber("525"));

        System.out.println("====Task5====");
        System.out.println(countSequenceOfCharacters(""));
        System.out.println(countSequenceOfCharacters("abc"));
        System.out.println(countSequenceOfCharacters("abbcca"));
        System.out.println(countSequenceOfCharacters("aaAAa"));


    }

    public static void fizzBuzz1(int num) {
        for (int i = 1; i <= num; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FizzBuzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }

    public static String fizzBuzz2(int num) {
        if (num % 3 == 0 && num % 5 == 0) return "FizzBuzz";
        else if (num % 3 == 0) return "Fizz";
        else if (num % 5 == 0) return "Buzz";
        else return String.valueOf(num);
    }

    public static int findSumNumbers(String str) {
        if (str.isEmpty()) {
            return 0;
        }

        String[] numbers = str.replaceAll("[^0-9]", " ").split(" ");
        int sum = 0;

        for (String num : numbers) {
            if (!num.isEmpty()) {
                sum += Integer.parseInt(num);
            }
        }

        return sum;
    }

    public static int findBiggestNumber(String str) {

        if (str.isEmpty()) {
            return 0;
        }

        String[] numbers = str.replaceAll("[^0-9]", " ").split(" ");
        int largestNumber = 0;

        for (String num : numbers) {
            if (!num.isEmpty() && (Integer.parseInt(num) > largestNumber)) {
                largestNumber = Integer.parseInt(num);
            }
        }
        return largestNumber;

    }

    public static String countSequenceOfCharacters(String str) {
        if(str.isEmpty()) return str;
        String answer = "";
        char letter = str.charAt(0);
        int count = 1;

        for (int i = 1; i < str.length(); i++) {
            if(str.charAt(i) == letter) count++;
            else{
                answer += Integer.toString(count) + letter;
                letter = str.charAt(i);
                count = 1;
            }
        }

        answer += Integer.toString(count) + letter;
        return answer;
    }

}

