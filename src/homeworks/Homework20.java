package homeworks;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework20 {
    public static void main(String[] args) {
        System.out.println("=====TASK1===========");
        int[] arr = {3, 7, 2, 5, 10};
        System.out.println(sum(arr, false));

        int[] arr1 = {1, 5, 10};
        System.out.println(sum(arr1, true));
        System.out.println("=====TASK2===========");
        System.out.println(sumDigitsDouble("Java"));
        System.out.println(sumDigitsDouble("23abc45"));
        System.out.println("=====TASK3===========");
        String firstString = "can Can";
        String secondString = "Anc";
        int occurrenceCount = countOccurrence(firstString, secondString);
        System.out.println("Occurrence Count: " + occurrenceCount);
    }

    public static int sum(int[] arr, boolean isEven) {
        int count = 0;
        for (int i : arr) {
            if (isEven && i % 2 == 0) count++;
            else if (!isEven & i % 2 == 1) count++;
        }
        return count;

    }

    public static int sumDigitsDouble(String str) {
        int result = 0;
        boolean hasDigits = false;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isDigit(c)) {
                int digit = Character.getNumericValue(c);
                result += digit * 2;
                hasDigits = true;
            }
        }
        if (!hasDigits) {
            return -1;
        }
        return result;
    }

   public static int countOccurrence(String str1, String str2) {
       str1 = str1.replaceAll("\\s", "").toLowerCase();
       str2 = str2.replaceAll("\\s", "").toLowerCase();
       int occurencecount = 0;
       char[] arr1 = str1.toCharArray();
       char[] arr2 = str2.toCharArray();
       for (int i = 0; i < arr1.length; i++) {

           if (arr2[i] == arr1[i]) occurencecount++;
           else return 0;

       }
return occurencecount;

   }
}