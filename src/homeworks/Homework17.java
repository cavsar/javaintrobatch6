package homeworks;

import java.util.List;
import java.util.Map;

public class Homework17 {
    public static void main(String[] args) {
        System.out.println("++++++++++++++Task1++++++++++++++++");
        System.out.println(nthWord("I like programming languages", 2));
        System.out.println(nthWord("QA stands for Quality Assurance", 4));
        System.out.println(nthWord("Hello World", 3));
        System.out.println("++++++++++++++Task2++++++++++++++++");
        System.out.println(isArmStrong(153));
        System.out.println(isArmStrong(123));
        System.out.println("++++++++++++++Task3++++++++++++++++");
        System.out.println(reverseNumber(371));
        System.out.println(reverseNumber(12));
    }

    public static String nthWord(String sentence, int count) {

        if (sentence.isEmpty()) {
            return "";
        }
        String[] result = sentence.split(" ");
        if (result.length < count) {
            return "";
        } else if (result.length > 0) {
            return result[count - 1];
        }
        return "result";
    }

    public static boolean isArmStrong(int number) {
        int originalNumber = number;
        int sum = 0;
        int numberOfDigits = String.valueOf(number).length();

        while (number >0) {
            int digit = number % 10;
            sum += Math.pow(digit, numberOfDigits);
            number /= 10;
        }

        return sum == originalNumber;
    }

    public static int reverseNumber(int num) {
        int reversedNumber = 0;

        while (num >0) {
            int lastDigit = num % 10;
            reversedNumber = reversedNumber * 10 + lastDigit;
            num /= 10;
        }

        return reversedNumber;
    }
}

