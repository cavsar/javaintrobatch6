package homeworks;

import java.util.HashMap;

public class Homework16 {
        public static void main(String[] args) {
            System.out.println("==========TASK01==========\n");
            System.out.println(parseData("{104}LA{101}Paris{102}Berlin{1033}Chicago{99}London"));
            System.out.println("\n==========TASK02==========\n");
            HashMap<String,Integer> data = new HashMap<>();
            data.put("Apple", 3);
            data.put("Mango", 1);
            System.out.println(calculateTotalPrice1(data));
            System.out.println("\n==========TASK03==========\n");
            HashMap<String,Integer> data1 = new HashMap<>();
            data1.put("Apple", 4);
            data1.put("Mango", 8);
            data1.put("Orange", 3);
            System.out.println(calculateTotalPrice2(data1));
        }

        public static HashMap<Integer,String> parseData(String str){//101 chicago 102 la
            HashMap<Integer, String> data = new HashMap<>();
            String[] arr = str.split("[{}]");//[,101,chicago,102,la]

            for (int i = 1; i < arr.length; i++) {
                data.put(Integer.parseInt(arr[i]),arr[i+1]);
                i++;
            }
            return data;
        }

        public static double calculateTotalPrice1(HashMap<String, Integer> data){
            double price = 0.0;

            for (String s : data.keySet()) {
                switch (s){
                    case "Apple":
                        price += data.get("Apple") * 2.00;
                        break;
                    case "Mango":
                        price += data.get("Mango") * 4.99;
                        break;
                    case "Orange":
                        price += data.get("Orange") * 3.29;
                        break;
                    case "Pineapple":
                        price += data.get("Pineapple") * 5.25;
                        break;
                }
            }
            return price;

        }

        public static double calculateTotalPrice2(HashMap<String, Integer> data){
            double price = 0;

            for (String s : data.keySet()) {
                switch (s){
                    case "Apple":

                        if(data.get("Apple") % 2 == 0) price += data.get("Apple") * 1.50;
                        else price += (data.get("Apple") * 1.50) + .50;
                        break;
                    case "Mango":
                        int freeMango = data.get("Mango")/4;
                        price += (data.get("Mango") * 4.99) - (freeMango * 4.99);
                        break;
                    case "Orange":
                        price += data.get("Orange") * 3.29;
                        break;
                }
            }
            return price;


        }
}
