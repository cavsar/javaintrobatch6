package homeworks;

import utilities.ScannerHelper;

public class Homework05 {
    public static void main(String[] args) {
        System.out.println("\n========TASK1==========\n");
        for (int i = 1; i <=100; i++) {
            if(i % 7 == 0) {
                if(i!=98) {System.out.print(i + "-" );
            }else {
                    System.out.println(i);
                }
        }}
        System.out.println("\n========TASK2==========\n");
        for (int i = 1; i <=50; i++) {
            if(i % 6 == 0) {
                if(i!= 48) {System.out.print(i + "-");
            }
            else {
                    System.out.println(i);
                }}}

        System.out.println("\n========TASK3==========\n");
        for (int i = 100; i >=50; i--) {
            if(i % 5 == 0){
                if (i!=50){System.out.print(i + "-");
            }else{
                    System.out.println(i);
                }}}

        System.out.println("\n========TASK4==========\n");
        for (int i = 0; i <=7; i++) {
            System.out.println("The square of " +i+" is =" + i*i);}

        System.out.println("\n========TASK5==========\n");
        int sum = 0;
        for (int i = 1; i <=10 ; i++) { sum += i;
        }
        System.out.println(sum);

        System.out.println("\n========TASK6==========\n");

        int num = ScannerHelper.getNumber();
        int factorial=1;
        for (int j = 1; j <=num ; j++) {
           factorial=factorial*j;
        }
        System.out.println(factorial);

        System.out.println("\n========TASK7==========\n");
        String fullName = ScannerHelper.getString().toLowerCase();
        int len= fullName.length();
        int count =0;
        for (int i = 0; i <=len-1 ; i++) {
            if(fullName.charAt(i) == 'a' || fullName.charAt(i) == 'e' ||
                    fullName.charAt(i) == 'o'||fullName.charAt(i) == 'u'
                    ||fullName.charAt(i) == 'i') count++;}
        System.out.println("There are " +count+ " vowel letters in this full name");

        System.out.println("\n========TASK8==========\n");
        String firstName;
        do{ firstName=ScannerHelper.getFirstName();}
        while(firstName.toLowerCase().startsWith("j"));}}



