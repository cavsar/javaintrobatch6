package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework10 {
    public static void main(String[] args) {
        System.out.println("=======Task1========");
        System.out.println(countWords("Java is fun"));
        System.out.println(countWords("Selenium is the most     common UI automation tool.   "));

        System.out.println("=======Task2========");
        System.out.println(countA("TechGlobal is a QA bootcamp"));
        System.out.println(countA("QA stands for Quality Assurance"));

        System.out.println("=======Task3========");
        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(-45, 0, 0, 34, 5, 67));
        System.out.println(countPos(list));
        ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(-23, -4, 0, 2, 5, 90, 123));
        System.out.println(countPos(list2));

        System.out.println("=======Task4========");
        System.out.println(Arrays.toString(removeDuplicateNumbers(new int[]{10, 20, 35, 20, 35, 60, 70, 60})));
        System.out.println(Arrays.toString(removeDuplicateNumbers(new int[]{1, 2, 5, 2, 3})));

        System.out.println("=======Task5========");
        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"))));
        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("abc", "xyz", "123", "ab", "abc", "ABC"))));

        System.out.println("=======Task6========");
        System.out.println(removeExtraSpaces("   I   am      learning     Java      "));

        System.out.println("=======Task7========");
        System.out.println(Arrays.toString(add(new int[]{3, 0, 0, 7, 5, 10}, new int[]{6, 3, 2})));
        System.out.println(Arrays.toString(add(new int[]{10, 3, 6, 3, 2}, new int[]{6, 8, 3, 0, 0, 7, 5, 10, 34})));

        System.out.println("=======Task8========");
        System.out.println(findClosestTo10(new int[]{10, -13, 5, 70, 15, 57}));
        System.out.println(findClosestTo10(new int[]{10, 12, 8, 12, 15, -20}));

    }

    public static int countWords(String str) {
        return str.trim().split("[\\s]+").length;
    }
      /*  int counter = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.contains(" ")) {
                str = str.substring(str.indexOf(" ") + 1).trim();
                counter++;

            }
        }
        return counter + 1;
    }

       */
/* -----------Regex Way------------------------
        Pattern pattern = Pattern.compile("[a-zA-Z]{1,}");
        Matcher matcher = pattern.matcher(str);
        int wordCount = 0;
        while (matcher.find()) {
            wordCount++;
        }
        return wordCount;
    }
 */

    public static int countA(String str) {
        int counterA = 0;
        String str1 = str.toLowerCase();
        for (int i = 0; i < str1.length(); i++) {
            if (str1.charAt(i) == 'a') counterA++;
        }
        return counterA;
    }
/* -----------Regex Way------------------------
    String str1=(str.replaceAll("[^aA]", ""));
    return str1.length();
}
 */

    public static int countPos(ArrayList<Integer> numbers) {
       return (int) numbers.stream().filter(e ->e>0).count();
        /*int counterPos = 0;
        for (Integer number : numbers) {
            if (number > 0) counterPos++;
        }
        return counterPos;

         */
    }

    public static Object[] removeDuplicateNumbers(int[] numbers) {
        ArrayList<Integer> list = new ArrayList<>();

        for (int i : numbers) {
            if (!list.contains(i)) list.add(i);
        }

        return list.toArray();
    }

    public static ArrayList<String> removeDuplicateElements(ArrayList<String> str) {
        ArrayList<String> list = new ArrayList<>();
        for (String s : str) {
            if (!list.contains(s)) list.add(s);
        }

        return list;
    }

    public static String removeExtraSpaces(String s) {
        String newStr = "";
        char[] charArr = s.toCharArray();
        for (int i = 0; i < charArr.length; i++) {
            if (charArr[i] != ' ') newStr += charArr[i];
            else if (i != charArr.length - 1 && charArr[i + 1] != ' ') newStr += charArr[i];
        }
        return newStr.trim();


        //  return s.trim().replaceAll("[\\s ]+", " ");

    }

    public static int[] add(int[] arr1, int[] arr2) {
        for (int i = 0; i < Math.min(arr1.length, arr2.length); i++) {
            if (arr1.length > arr2.length) arr1[i] += arr2[i];
            else arr2[i] += arr1[i];
        }
        return arr1.length > arr2.length ? arr1 : arr2;
    }

    public static int findClosestTo10(int[] arr) {
      /*  Arrays.sort(arr);
        int numberBefore10 = 0;// 5  11-5 = 6
        int numberAfter10 = 0;// 15 10-15 = 4

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 10) numberBefore10 = arr[i];
            else break;
        }
        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] > 10) numberAfter10 = arr[i];
            else break;
        }
            if (Math.abs(10 - numberBefore10) <= Math.abs(10 - numberAfter10)) return numberBefore10;
            else return numberAfter10;
    }
       */
        Arrays.sort(arr);// otherwise it will take 12 first
        int closest = Integer.MAX_VALUE;
        for (int number : arr) {
            if (number != 10 && Math.abs(number - 10) < Math.abs(closest - 10)) {
                closest = number;
            }
        }
        return closest;
    }}
