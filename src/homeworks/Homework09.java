package homeworks;

import java.util.ArrayList;
import java.util.Collections;

public class Homework09 {
    public static void main(String[] args) {
        System.out.println("========Task1===========");

        int[] arr = {3, 4, 3, 3, 5, 5, 6, 6, 7};


        ArrayList<Integer> duplicates = new ArrayList<>();
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (!duplicates.contains(arr[j]) && arr[i] == arr[j]) {
                    duplicates.add(arr[i]);
                    break;
                }
            }
        }
        if (duplicates.size() == 0) {
            System.out.println("There is no duplicates");
            ; // no duplicates found
        } else {
            System.out.println(duplicates.get(0)); // return the first duplicate
        }
        System.out.println("========Task2===========");
        String[] words = {"Z", "abc", "aBc", "123", "#"};
        ArrayList<String> dup = new ArrayList<>();
        for (int i = 0; i < words.length - 1; i++) {
            for (int j = i + 1; j < words.length; j++) {
                if (!dup.equals("" + words[i]) && words[i].equalsIgnoreCase(words[j])) {
                    dup.add(words[i]);
                    break;
                }
            }

        }
        if (dup.size() == 0) {
            System.out.println("There is no duplicates");
            ; // no duplicates found
        } else {
            System.out.println(dup.get(0)); // return the first duplicate

        }
        System.out.println("========Task3===========");
        int[] numbers = {0, -4, -7, 0, 5, 10, 45, -7, 0};
        ArrayList<Integer> dupl = new ArrayList<>();
        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = i + 1; j < numbers.length; j++) {
                if (!dupl.contains(numbers[i]) && numbers[i] == numbers[j]) {
                    dupl.add(numbers[i]);
                }
            }
        }
        if (dupl.size() == 0) {
            System.out.println("There is no duplicates");
            ; // no duplicates found
        } else {
            System.out.println(dupl); //

        }
        System.out.println("========Task4===========");
        String[] w = {"A", "foo", "12", "Foo", "bar", "a", "a", "java"};
        ArrayList<String> dupl2 = new ArrayList<>();
        for (int i = 0; i < w.length - 1; i++) {
            for (int j = i + 1; j < w.length; j++) {
                if (!dupl2.contains(w[i].toLowerCase()) && w[i].equalsIgnoreCase(w[j])) {
                    dupl2.add(w[i].toLowerCase());
                    break;
                }
            }
        }
        if (dupl2.size() == 0) {
            System.out.println("There is no duplicates");
            ; // no duplicates found
        } else {
            System.out.println(dupl2); //

        }
        System.out.println("========Task5===========");
        String[] words1 = {"abc", "foo", "bar"};
        ArrayList<String> list = new ArrayList<>();

        for (int i = words1.length - 1; i >= 0; i--) {
            list.add(words1[i]);
        }
        System.out.println(list);
        System.out.println("========Task6===========");
        String str = "Java is fun";
        String result = "";

        String reversedWord = "";
        for (int i = str.length() - 1; i >= 0; i--) {
            reversedWord += str.charAt(i);
        }
        result += reversedWord + " ";
        System.out.println(result);
    }
}




