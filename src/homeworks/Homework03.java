package homeworks;

import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

       System.out.println("\n==========TASK1==========\n");
        int num1, num2;
        System.out.println("Please enter 2 numbers");
        num1 = input.nextInt();
        num2 = input.nextInt();
        System.out.println("The difference between numbers is = " + Math.abs(num1-num2));

        System.out.println("\n==========TASK2==========\n");
        int number1, number2, number3, number4, number5;
        System.out.println("Please enter 5 numbers");
        number1 = input.nextInt();
        number2 = input.nextInt();
        number3 = input.nextInt();
        number4 = input.nextInt();
        number5 = input.nextInt();
        System.out.println("Max value = "+ Math.max(Math.max(Math.max(number1,number2),Math.max(number3,number4)),number5));
        System.out.println("Min value = "+ Math.min(Math.min(Math.min(number1,number2),Math.min(number3,number4)),number5));

        System.out.println("\n==========TASK3==========\n");
        int n1 = (int)(Math.random()*51)+50;
        int n2 = (int)(Math.random()*51)+50;
        int n3 = (int)(Math.random()*51)+50;
        System.out.println("Number 1 ="+ n1);
        System.out.println("Number 2 ="+ n2);
        System.out.println("Number 3 ="+ n3);
        System.out.println("The sum of numbers is "+ (n1+n2+n3));

        System.out.println("\n==========TASK4==========\n");
        double TheMoneyOfAlex =125;
        double TheMoneyOfMike =220;
        double TheMoneyOfAlex1 = TheMoneyOfAlex - 25.5;
        double TheMoneyOfMike1 = TheMoneyOfMike + 25.5;
        System.out.println("Alex's money " + "$"+TheMoneyOfAlex1);
        System.out.println("Mike's money " + TheMoneyOfMike1);


        System.out.println("\n==========TASK5==========\n");

        double price = 390;
        double savingPerDay =15.60;
        int day= (int) (price/savingPerDay);
        System.out.println(day);

        System.out.println("\n==========TASK6==========\n");

        String s1 = "5";
        String s2 ="10";
        int m1 = Integer.parseInt(s1);
        int m2 = Integer.parseInt(s2);
        System.out.println("Sum of " + s1+ "and "+s2+" is = " + ( m1 + m2));
        System.out.println("Product of " + s1+ "and "+s2+" is = " + ( m1 * m2));
        System.out.println("Divison of " + s1+ "and "+s2+" is = " + ( m1 / m2));
        System.out.println("Subtraction of " + s1+ "and "+s2+" is = " + ( m1 - m2));
        System.out.println("Remainder of " + s1+ "and "+s2+" is = " + ( m1 % m2));


        System.out.println("\n==========TASK7==========\n");

        String s3 = "200";
        String s4 = "-50";
        int r3 = Integer.parseInt(s3);
        int r4 = Integer.parseInt(s4);
        System.out.println("The greatest value is = " + Math.max( r3,r4 ));
        System.out.println("The smallest value is = " +Math.min(r3, r4));
        System.out.println("The average is = " + (r3 +r4)/2);
        System.out.println( "The absolute difference is = " + Math.abs(r3-r4));


        System.out.println("\n==========TASK8==========\n");
        double dailySavings= (3*0.25)+(1*0.10)+(2*0.05)+0.01;
        double saving1 =24;
        double saving2 =168;
        int duration =5*30;

        System.out.println ((int) (saving1 / dailySavings) + " days.");
        System.out.println((int) (saving2 / dailySavings) + " days.");
        System.out.println("$"+(double)(duration*dailySavings));


        System.out.println("\n==========TASK9==========\n");

        double price1 = 1250;
        double savingPerDayx = 62.50;

        System.out.println((int) (price1/savingPerDayx));


        System.out.println("\n==========TASK10==========\n");

        //option 1-->$475
        //option 2-->$951
        //price----->$14,265


        double priceOfCar = 14265;
        double option1 =475;
        double option2 =951;

        System.out.println("Option 1 will take " +((int) (priceOfCar/option1) +" months"));
        System.out.println("Option 2 will take " +((int) (priceOfCar/option2) +" months"));


        System.out.println("\n==========TASK11==========\n");
        int x, y;
        System.out.println("Please enter 2 numbers");
        x= input.nextInt();
        y= input.nextInt();
        System.out.println((double)(x/y));


        System.out.println("\n==========TASK12==========\n");
         int a= (int) (Math.random() * 101);
         int b= (int) (Math.random() * 101);
         int c= (int) (Math.random() * 101);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);

        if (a>25 && b>25 && c>25) {
                    System.out.println("true");}
                else {
                    System.out.println("false");}
        System.out.println("End of the program");


         System.out.println("\n==========TASK13==========\n");

         System.out.println("Please enter a number from 1 to 7");
         int dayOfWeek = input.nextInt();
         if ( dayOfWeek ==1 ) {
             System.out.println("The number entered returns MONDAY");}
         else if (dayOfWeek ==2 ) {       System.out.println("The number entered returns TUESDAY");}
         else if (dayOfWeek ==3 ) {       System.out.println("The number entered returns WEDNESDAY"); }
         else if (dayOfWeek ==4 ) {       System.out.println("The number entered returns THURSDAY");}
         else if (dayOfWeek ==5 ) {       System.out.println("The number entered returns FRIDAY");}
         else if (dayOfWeek ==6 ) {       System.out.println("The number entered returns SATURDAY"); }
         else if (dayOfWeek ==7) {           System.out.println("The number entered returns SUNDAY");
    }
        System.out.println( "The end of the program");
        

            System.out.println("\n==========TASK14==========\n");
            int grade1, grade2, grade3;

            System.out.println("Tell me your exam results?");
            grade1 = input.nextInt();
            grade2 = input.nextInt(); 
            grade3 = input.nextInt();
            int average =  (grade1 + grade2 + grade3) /3;

            if (average>= 70) {
                System.out.println("YOU PASSED!");
            }
            else {
                System.out.println("YOU FAILED!");    }
        System.out.println("End of the program");



              System.out.println("\n==========TASK15==========\n");
        System.out.println("Please enter 3 numbers");
        int a1, a2, a3;
        a1 = input.nextInt();
        a2 = input.nextInt();
        a3 = input.nextInt();

       if ((a1!=a2) && (a2!=a3) && (a1!=a3)) {
           System.out.println("NO MATCH");}
       else if ((a1==a2)||(a1==a3)|| (a2==a3)) {
           System.out.println("DOUBLE MATCH");}
       else {
           System.out.println("TRIPLE MATCH");  }
        System.out.println("End of the program");

    }  }