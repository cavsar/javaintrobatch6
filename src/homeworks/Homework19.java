package homeworks;

import java.util.Arrays;

public class Homework19 {
    public static void main(String[] args) {
        System.out.println("==========TASK1=========");
        int[] arr = {3, 7, 2, 5, 10};
        boolean isEven = false;
        int result = sum(arr, isEven);
        System.out.println(result);  // Output: 12
        int[] arr1={1, 5, 10};
        boolean isEven1 =true;
        int result1 =sum(arr1,isEven1);
        System.out.println(result1); // Output: 11

        System.out.println("==========TASK2=========");
        System.out.println(nthChars("Java", 2));
        System.out.println(nthChars("JavaScript", 5));
        System.out.println(nthChars("Java", 3));
        System.out.println(nthChars("hi", 2));

        System.out.println("==========TASK3=========");
        System.out.println(canFormString("programming", "gaming"));

        System.out.println("==========TASK4=========");
        System.out.println(isAnagram("listen", "silent"));

    }
    public static int sum(int[]arr, boolean isEven){
        int sum = 0;
        int firstIndex;
        if (isEven) {
            firstIndex = 0;
        } else {
            firstIndex = 1;
        }
        for (int i = firstIndex; i < arr.length; i += 2) {
            sum += arr[i];
        }
        return sum;
    }
    public static String nthChars(String str, int num){
        if (str.length() < num) {
            return "";
        }

        String result = "";
        for (int i = num - 1; i < str.length(); i += num) {
            result += str.charAt(i);
        }

        return result;

    }
    public static boolean canFormString(String str1, String str2) {

        // Remove white spaces and convert both strings to lowercase
    str1 = str1.replaceAll("\\s", "").toLowerCase();
    str2 = str2.replaceAll("\\s", "").toLowerCase();

    // Convert strings to char arrays and sort them
    char[] arr1 = str1.toCharArray();
    char[] arr2 = str2.toCharArray();
        Arrays.sort(arr1);
        System.out.println(arr1);
        Arrays.sort(arr2);
        System.out.println(arr2);

    // Compare the sorted char arrays
        return Arrays.equals(arr1, arr2);
}
    public static boolean isAnagram(String str1, String str2) {
        // Remove white spaces and convert to lowercase
        String s1 = str1.replaceAll("\\s", "").toLowerCase();
        String s2 = str2.replaceAll("\\s", "").toLowerCase();

        // Check if the lengths are equal
        if (s1.length() != s2.length()) {
            return false;
        }

        // Convert strings to character arrays
        char[] charArray1 = s1.toCharArray();
        char[] charArray2 = s2.toCharArray();

        // Sort the character arrays
        Arrays.sort(charArray1);
        Arrays.sort(charArray2);
        // Sort the character arrays
        Arrays.sort(charArray1);
        Arrays.sort(charArray2);

        // Compare the sorted arrays
        return Arrays.equals(charArray1, charArray2);
    }



}
