package homeworks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class Homework07 {
    public static void main(String[] args) {
        System.out.println("\n-------Task1-------\n");
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(10);
        numbers.add(23);
        numbers.add(67);
        numbers.add(23);
        numbers.add(78);
        System.out.println(numbers.get(3));
        System.out.println(numbers.get(0));
        System.out.println(numbers.get(2));
        System.out.println(numbers);

        System.out.println("\n-------Task2-------\n");
        ArrayList<String> colors = new ArrayList<>();
        colors.add("Blue");
        colors.add("Brown");
        colors.add("Red");
        colors.add("White");
        colors.add("Black");
        colors.add("Purple");
        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));
        System.out.println(colors);

        System.out.println("\n-------Task3-------\n");
        ArrayList<Integer> n = new ArrayList<>();
        n.add(23);
        n.add(-34);
        n.add(-56);
        n.add(0);
        n.add(89);
        n.add(100);
        System.out.println(n);
        Collections.sort(n);
        System.out.println(n);

        System.out.println("\n-------Task4-------\n");
        ArrayList<String> cities = new ArrayList<>();
        cities.add("Istanbul");
        cities.add("Berlin");
        cities.add("Madrid");
        cities.add("Paris");
        System.out.println(cities);
        Collections.sort(cities);
        System.out.println(cities);

        System.out.println("\n-------Task5-------\n");
        ArrayList<String> MarvelCharacters = new ArrayList<>();
        MarvelCharacters.add("Spider Man");
        MarvelCharacters.add("Iron Man");
        MarvelCharacters.add("Black Panter");
        MarvelCharacters.add("Deadpool");
        MarvelCharacters.add("Captain America");
        System.out.println(MarvelCharacters);
        System.out.println((MarvelCharacters.contains("Wolwerine")));

        System.out.println("\n-------Task6-------\n");
        ArrayList<String> AvengersCharacters = new ArrayList<>();
        AvengersCharacters.add("Hulk");
        AvengersCharacters.add("Black Widow");
        AvengersCharacters.add("Captain America");
        AvengersCharacters.add("Iron Man");
        Collections.sort(AvengersCharacters);
        System.out.println(AvengersCharacters);
        System.out.println((AvengersCharacters.contains("Hulk") && AvengersCharacters.contains("Iron Man")));


        System.out.println("\n-------Task7-------\n");
        ArrayList<Character> characters = new ArrayList<>();
        characters.add('A');
        characters.add('x');
        characters.add('$');
        characters.add('%');
        characters.add('9');
        characters.add('*');
        characters.add('+');
        characters.add('F');
        characters.add('G');
        System.out.println(characters);
        for (Character character : characters) {
            System.out.println(character);

            System.out.println("\n-------Task8-------\n");
            ArrayList<String> objects = new ArrayList<>();
            objects.add("Desk");
            objects.add("Laptop");
            objects.add("Mouse");
            objects.add("Monitor");
            objects.add("Mouse-Pad,");
            objects.add("Adapter");
            System.out.println(objects);
            Collections.sort(objects);
            System.out.println(objects);
            int countM = 0;
            for (String object : objects) {
                if (object.toLowerCase().startsWith("m")) countM++;
            }
            System.out.println(countM);
            int countAorE = 0;
            for (String object : objects) {
                if (!object.toLowerCase().contains("a")
                        && !object.toLowerCase().contains("e")) countAorE++;
            }
            System.out.println(countAorE);

            System.out.println("\n-------Task9-------\n");
            ArrayList<String> kitchenObjects = new ArrayList<>();
            kitchenObjects.add("Plate");
            kitchenObjects.add("spoon");
            kitchenObjects.add("fork");
            kitchenObjects.add("Knife");
            kitchenObjects.add("cup");
            kitchenObjects.add("Mixer");
            System.out.println(kitchenObjects);

            int countUpper = 0;
            int countLower = 0;
            int hasP = 0;
            int startsAndEndsP = 0;
            for (String kitchenObject : kitchenObjects) {
                if (kitchenObject.startsWith("p") || kitchenObject.startsWith("P") || kitchenObject.endsWith("p") ||
                        kitchenObject.endsWith("P")) startsAndEndsP++;
                if (kitchenObject.contains("p") || kitchenObject.contains("P")) hasP++;
                if (kitchenObject.charAt(0) >= 65 && kitchenObject.charAt(0) <= 90) countUpper++;
                if (kitchenObject.charAt(0) >= 97 && kitchenObject.charAt(0) <= 122) countLower++;

            }
            System.out.println("Elements starts with uppercase =" + countUpper);
            System.out.println("Elements starts with lowercase = " + countLower);
            System.out.println("Elements having P or p=" + hasP);
            System.out.println("Elements starting or ending with P or p = " + startsAndEndsP);

            System.out.println("\n-------Task10-------\n");
            ArrayList<Integer> num = new ArrayList<>();
            num.add(3);
            num.add(5);
            num.add(7);
            num.add(10);
            num.add(0);
            num.add(20);
            num.add(17);
            num.add(10);
            num.add(23);
            num.add(56);
            num.add(78);
            System.out.println(num);
            int divided10 = 0, evenGreater15 = 0, oddLess20 = 0, less15OrGreater50 = 0;
            for (Integer integer : num) {
                if (integer % 10 == 0) divided10++;
                if (integer % 2 == 0 && integer > 15) evenGreater15++;
                if (integer % 2 == 1 && integer < 20) oddLess20++;
                if (integer < 15 || integer > 50) less15OrGreater50++;
            }
            System.out.println("Elements that can be divided by 10 = " + divided10);
            System.out.println("Elements that are even and greater than 15 = " + evenGreater15);
            System.out.println("Elements that are odd and less than 20 = " + oddLess20);
            System.out.println("Elements that are less than 15 or greater than 50 = " + less15OrGreater50);
        }
    }


}

