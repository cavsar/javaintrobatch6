package homeworks;

import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {
        System.out.println("\n------TASK1-------\n");
        int[] numbers = new int[10];
        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;

        System.out.println(numbers[3]);
        System.out.println(numbers[0]);
        System.out.println(numbers[9]);
        System.out.println(Arrays.toString(numbers));

        System.out.println("\n------TASK2-------\n");
        String[] s = new String[5];
        s[1] = "abc";
        s[4] = "xyz";

        System.out.println(s[3]);
        System.out.println(s[0]);
        System.out.println(s[4]);
        System.out.println(Arrays.toString(s));

        System.out.println("\n------TASK3-------\n");
        int num[] = {23, -34, -56, 0, 89, 100};
        System.out.println(Arrays.toString(num));
        Arrays.sort(num);
        System.out.println(Arrays.toString(num));

        System.out.println("\n------TASK4-------\n");
        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};
        System.out.println(Arrays.toString(countries));
        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));

        System.out.println("\n------TASK5-------\n");
        String[] cartoonsdogs = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};
        System.out.println(Arrays.toString(cartoonsdogs));
        for (String cartoonsdog : cartoonsdogs) {
            System.out.println(!cartoonsdog.contains("Pluto"));
            break;
        }
        System.out.println("\n------TASK6-------\n");
        String[] cartoonscats = {"Garfield", "Tom", "Sylvester", "Azrael"};
        Arrays.sort(cartoonscats);
        System.out.println(Arrays.toString(cartoonscats));
        for (String cartoonscat : cartoonscats) {
            System.out.println((cartoonscat.contains("Garfield") && cartoonscat.contains("Felix")));
            break;
        }
        System.out.println("\n------TASK7-------\n");
        double[] doubles = {10.5, 20.75, 70, 80, 15.75};
        System.out.println(Arrays.toString(doubles));
        for (double aDouble : doubles) {
            System.out.println(aDouble);

        }
        System.out.println("\n------TASK8-------\n");
        char[] chars = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};
        System.out.println(Arrays.toString(chars));

        int counter = 0;
        for (char aChar : chars) {
            if (Character.isLetter(aChar)) counter++;
        }
        System.out.println("Letters = " + counter);
        counter = 0;
        for (char aChar : chars) {
            if (Character.isUpperCase(aChar)) counter++;

        }
        System.out.println("Uppercase letters =" + counter);
        counter = 0;
        for (char aChar : chars) {
            if (Character.isLowerCase(aChar)) counter++;

        }
        System.out.println("Lowercase letters = " + counter);
        counter = 0;
        for (char aChar : chars) {
            if (Character.isDigit(aChar)) counter++;

        }
        System.out.println("Digits = " + counter);
        counter = 0;
        for (char aChar : chars) {
            if (!Character.isLetterOrDigit(aChar) && !Character.isWhitespace(aChar)) counter++;

        }
        System.out.println("Special character = " + counter);

        System.out.println("\n------TASK9-------\n");
        String[] objects = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};
        System.out.println(Arrays.toString(objects));
        int count = 0;
        for (String object : objects) {
            if (Character.isUpperCase(object.charAt(0))) count++;

        }
        System.out.println("Elements starts with uppercase = " + count);
        count = 0;
        for (String object : objects) {
            if (Character.isLowerCase(object.charAt(0))) count++;

        }
        System.out.println("Elements starts with lowercase = " + count);
        count = 0;
        for (String object : objects) {
            if (object.startsWith("B") || object.startsWith("p") || object.startsWith("b") || object.startsWith("P"))
                count++;

        }
        System.out.println("Elements starting with B or P = " + count);

        count=0;
        for (String object : objects) {
            if(object.toLowerCase().contains("book")|| object.toLowerCase().contains("pen"))count++;

        }
        System.out.println("Elements having ”book” or “pen”= "+count);

        System.out.println("\n========TASK10=======\n");
        int[] numbers10= {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};
        System.out.println(Arrays.toString(numbers10));

        count =0;
        for (int i : numbers10) {
            if(i>10) count++;
        }
        System.out.println("Elements that are more than 10 = " +count);

        count =0;
        for (int i : numbers10) {
            if(i<10) count++;
        }
        System.out.println("Elements that are less than 10 = " +count);

        count =0;
        for (int i : numbers10) {
            if(i==10) count++;
        }
        System.out.println("Elements that are 10 = " +count);

        System.out.println("\n========TASK11=======\n");
        int[] arr1={5, 8, 13, 1, 2};
        int[] arr2={9, 3, 67, 1, 0};
        System.out.println("1st array is = "+Arrays.toString(arr1));
        System.out.println("2nd array is = "+Arrays.toString(arr2));
        int[] third = new int[5];
        for (int i = 0; i < arr1.length; i++) {
            third[i] = Math.max(arr1[i], arr2[i]);
        }
        System.out.println("3rd array is = " + Arrays.toString(third));

    }

        }


