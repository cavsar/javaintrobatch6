package homeworks;

import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {
    public static void main(String[] args) {
        System.out.println("++++++Task1++++++");
        System.out.println((countConsonsonants("hello")));

        System.out.println("++++++Task2++++++");
        System.out.println(Arrays.toString(wordArray("hello")));
        System.out.println(Arrays.toString(wordArray("java is fun")));
        System.out.println(Arrays.toString(wordArray("Hello, nice to meet you!!")));

        System.out.println("++++++Task3++++++");
        System.out.println(removeExtraSpaces("Hello,    nice to   meet     you!!"));

        System.out.println("++++++Task4++++++");
        System.out.println(count3OrLess(""));

        System.out.println("++++++Task5++++++");
        System.out.println(isDateFormatValid("01/21/1991"));

        System.out.println("++++++Task6++++++");
        System.out.println(isEmailFormatValid("abc@student.techglobal.com"));

    }

    public static int countConsonsonants(String str) {
        str = str.replaceAll("[aeiouAEIOU]", "");//Apple -> Ae

        return str.length();
    }
    public static String[] wordArray(String str) {
        return str.split("\\s+");
    }
    public static String removeExtraSpaces(String str){
        str=str.replaceAll("\\s+", " ").trim();
        return str;
    }
    public static int count3OrLess(String str){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a sentence: ");
        String sentence = scanner.nextLine();
        Pattern pattern = Pattern.compile("\\b\\w{1,3}\\b");
        Matcher matcher = pattern.matcher(sentence);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }
    public static boolean isDateFormatValid(String str){
       String DOB= "\"[0-1]{1}[0-2]{1}\\\\/[0-3]{1}[0-1]{1}\\\\/[0-9]{4}\"";


        return true;
    }
    public static boolean isEmailFormatValid (String str){
        String EmailRegex="\"[a-zA-Z0-9]{2,}\\@\\[a-zA-Z0-9}{2,}\\.\\[a-zA-Z0-9]{2,}\"";


        return true;
    }
    }

