package homeworks;

public class Homework01 {
    public static void main(String[] args) {
        System.out.println("\n---------TASK1---------\n");

        /*
        JAVA = 01001010;01000001;01010110;01000001=
        SELENIUM =01010011;01000101;01001100;01000101;01001110;01001001;01010101;01001101
        */

        System.out.println("\n---------TASK2---------\n");

        /*
        01001101=64+8+4+1=77 //M
        01101000=64+32+8=104 //h
        01111001=64+32+16+8+1=121 //y
        01010011=64+16+2+1=83 //S
        01101100=64+32+8+4=108 //l
        */

        System.out.println("\n---------TASK3---------\n");

        System.out.println("I start to practice \"JAVA\" today, and I like it.");
        System.out.println("The secret of getting ahead is getting started.");
        System.out.println("\"Don't limit yourself. \"");
        System.out.println("Invest in your dreams. Grind now. Shine later.");
        System.out.println("It’s not the load that breaks you down, it’s the way you carry \nit.");
        System.out.println("The hard days are what make you stronger.");
        System.out.println("You can waste your lives drawing lines. Or you can live your life crossing them.");

        System.out.println("\n---------TASK4---------\n");

        System.out.println("\tJava is easy to write and easy to run\nthis is the foundational\nstrength of Java and why many developers program in it. When you \nwrite Java once, you can run it almost anywhere at any time.\n\n\tJava can be used to create complete applications that can run on\na single computer or be distributed across servers and clients in a\nnetwork.\n\n\tAs a result, you can use it to easily build mobile applications or\nrun-on desktop applications that use different operating systems and\nservers, such as Linux or Windows");
        System.out.println("\n---------TASK5---------\n");
        byte myAge = 38;
        byte myFavoriteNumber= 21;
        float myHeight = 1.58F;
        byte myWeight=65;
        char myFavoriteLetter = 'J';


        System.out.println(myAge);
        System.out.println(myFavoriteNumber);
        System.out.println(myHeight);
        System.out.println(myWeight);
        System.out.println(myFavoriteLetter);


    }
}
