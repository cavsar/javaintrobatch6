package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework13 {
    public static void main(String[] args) {
        System.out.println("=====Task1======");
        System.out.println(hasLowerCase(""));
        System.out.println(hasLowerCase("JAVA"));
        System.out.println(hasLowerCase("125$"));
        System.out.println(hasLowerCase("hello"));
        System.out.println("=====Task2======");
        System.out.println(noZero(new ArrayList<>(Arrays.asList(1))));
        System.out.println(noZero(new ArrayList<>(Arrays.asList(1, 1, 10))));
        System.out.println(noZero(new ArrayList<>(Arrays.asList(0, 1, 10))));
        System.out.println(noZero(new ArrayList<>(Arrays.asList(0, 0, 0))));
        System.out.println("=====Task3======");
        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{1, 2, 3})));
        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{0, 3, 6})));
        System.out.println("=====Task4======");
        System.out.println(containsValue(new String[]{"abc", "foo", "java"}, "hello"));
        System.out.println(containsValue(new String[]{"abc", "def", "123"}, "Abc"));
        System.out.println(containsValue(new String[]{"abc", "def", "java"}, "def"));
        System.out.println("=====Task5======");
        System.out.println(reverseSentence("Java is fun"));
        System.out.println(reverseSentence("Hello"));
        System.out.println(reverseSentence("This is a sentence"));
        System.out.println("=====Task6======");
        System.out.println(removeStringSpecialsDigits("123Java #$%is fun"));
        System.out.println(removeStringSpecialsDigits("Selenium"));
        System.out.println(removeStringSpecialsDigits("Selenium 123#$%Cypress"));
        System.out.println("=====Task7======");
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"123AbC", "xy z #$%", "abc"})));
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"Selenium", "123$%", "###"})));
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"123AbC", "xy z #$%Cypress"})));
        System.out.println("=========Task8========");
        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "is", "fun")), new ArrayList<>(Arrays.asList("abc", "xyz", "123"))));
        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "is", "fun")), new ArrayList<>(Arrays.asList("Java", "C#", "Python"))));
        System.out.println(removeAndReturnCommons(new ArrayList<>(Arrays.asList("Java", "C#", "C#")), new ArrayList<>(Arrays.asList("Python", "", "C#", "C++"))));
        System.out.println("=========Task9========");
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("abc", "123#$%"))));
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("xyz","123","#$%"))));
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("x", "123","#$%"))));
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("xyXyxy", "Xx", "ABC"))));


    }

    public static boolean hasLowerCase(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (Character.isLowerCase(str.charAt(i))) return true;


        }
        return false;
    }

    public static ArrayList<Integer> noZero(ArrayList<Integer> list) {
        ArrayList<Integer> listResult = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) != 0) listResult.add(list.get(i));

        }
        return listResult;

    }

    public static int[][] numberAndSquare(int[] array) {
        int[][] result = new int[array.length][2];

        for (int i = 0; i < array.length; i++) {
            result[i][0] = array[i];  // original number
            result[i][1] = array[i] * array[i];  // squared number
        }

        return result;

    }

    public static boolean containsValue(String[] arr, String str) {
        return Arrays.asList(arr).contains(str);
    }

    public static String reverseSentence(String str) {
        String str1 = "";
        String[] strArr = str.trim().split("[ ]+");
        strArr[0] = strArr[0].toLowerCase();

        if (strArr.length < 2) return "There's not enough words!";
        for (int i = strArr.length - 1; i >= 0; i--) {
            str1 += strArr[i] + " ";
        }
        return str1.substring(0, 1).toUpperCase() + str1.substring(1, str1.length() - 1);


    }

    public static String removeStringSpecialsDigits(String str) {
        return str.replaceAll("[^a-zA-Z ]", "");
    }

    public static String[] removeArraySpecialsDigits(String[] arr) {

        String[] answer = new String[arr.length];

        //123AbC
        for (int i = 0; i < arr.length; i++) {
            String str = "";
            for (int j = 0; j < arr[i].length(); j++) {
                if (Character.isLetter(arr[i].charAt(j)) || Character.isWhitespace(arr[i].charAt(j)))
                    str += arr[i].charAt(j);
            }
            answer[i] = str;
        }
        return answer;
    }

    public static ArrayList<String> removeAndReturnCommons(ArrayList<String> arr1, ArrayList<String> arr2) {
        ArrayList<String> duplicates = new ArrayList<>();
        for (String s1 : arr1) {
            for (String s2 : arr2) {
                if (s1.equals(s2) && !duplicates.contains(s1)) duplicates.add(s1);
            }
        }
        return duplicates;
    }

    public static ArrayList<String> noXInVariables(ArrayList<String> arr) {
        for (int i = 0; i < arr.size(); i++) {
            arr.set(i, arr.get(i).replaceAll("[xX]", ""));
            if (arr.get(i).isEmpty()) {
                arr.remove(i);
                i--;
            }
        }
        return arr;

    }


}




