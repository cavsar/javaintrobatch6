package homeworks;


import java.util.Scanner;

public class Homework02 {

    public static void main(String[] args) {
        System.out.println("\n===========TASK1============\n");

        Scanner InputReader = new Scanner(System.in);


        System.out.println( "Please enter the number 1");
        int num1= InputReader. nextInt();
        System.out.println( "The number 1 entered by user is = " +num1);
        System.out.println( "Please enter the number 2");
        int  num2 = InputReader.nextInt();
        System.out.println( "The number 2 entered by user is = " +num2);
        int sum = num1 +num2;



        System.out.println( "The sum of number 1 and 2 entered by user is = "+ (num1 + num2));

        System.out.println("\n===========TASK2============\n");
        System.out.println( "Please enter the number 1");
        int a1 = InputReader.nextInt();
        System.out.println( "Please enter the number 2");
        int a2 = InputReader.nextInt();


        System.out.println( "The product of the given 2 numbers is:  = "+ a1*a2);

        System.out.println("\n===========TASK3============\n");
        System.out.println( "Please enter the number 1");
        double d1 = InputReader. nextDouble();
        System.out.println( "Please enter the number 2");
        double d2 = InputReader. nextDouble();
        System.out.println("The sum of the given numbers is "+(d1+d2)+
                "\nThe product of the given numbers is = "+d1*d2 +
                "\nThe subtraction of the given numbers is = "+(d1-d2) +
                "\nThe division of the given numbers is = "+ d1/d2+
                "\nThe remainder of the given numbers is "+ d1%d2);

        System.out.println("\n===========TASK4============\n");
        System.out.println(-10+7*5);
        System.out.println((72+24) % 24);
        System.out.println(10 + -3*9 / 9);
        System.out.println(5 + 18 / 3 * 3- (6 % 3));

        System.out.println("\n===========TASK5============\n");
        int a = InputReader.nextInt();
        int b = InputReader.nextInt();

        System.out.println( "The average of the given numbers is: "+(a+b)/2);

        System.out.println("\n===========TASK6============\n");
        int c1 = InputReader.nextInt();
        int c2 = InputReader.nextInt();
        int c3 = InputReader.nextInt();
        int c4 = InputReader.nextInt();
        int c5 = InputReader.nextInt();
        System.out.println("The average of the given numbers is: "+(c1+c2+c3+c4+c5)/5);

        System.out.println("\n===========TASK7============\n");
        System.out.println( "Please enter the number 1");
        int a4 = InputReader.nextInt();
        System.out.println( "Please enter the number 2");
        int a5 = InputReader.nextInt();
        System.out.println( "Please enter the number 3");
        int a3 = InputReader.nextInt();
        System.out.println("The "+a4 + " multiplied with 5 is = "+ a4*5+
                "\nThe " + a5 + " multiplied with 6 is = "+ a5*6+
                "\nThe " + a3 + " multiplied with 10 is = "+a3*10);

     System.out.println("\n===========TASK8============\n");
     int side = InputReader.nextInt();
     System.out.println( "Please enter the side of a square ");
     System.out.println("Perimeter of the square = "+ 4*side +
             "\nArea of the square = "+side*side);

     System.out.println("\n===========TASK9============\n");
     double averageSalary = 90000.0;
     double salaryPer3Years = averageSalary * 3;

     System.out.println("A Software Engineer in Test can earn $" + salaryPer3Years + " in 3 years.");

     System.out.println("\n===========TASK10============\n");
        String favBook, favColor;
        int favNumber ;

        System.out.println(" Please enter your favorite book: ");
        favBook = InputReader.nextLine();

        System.out.println(" Please enter your favorite color: ");
        favColor = InputReader.nextLine();

        System.out.println(" Please enter your favorite number: ");
        favNumber = InputReader.nextInt();
        InputReader.nextLine();

        System.out.println("User's favorite book is: "+favBook+
             "\nUser's favorite color is: "+favColor+
             "\nUser's favorite number is: "+favNumber);

     System.out.println("\n===========TASK11============\n");
     Scanner input =new Scanner(System.in);
     String firstName, lastName, emailAddress, address, phoneNumber;
     int age;


        System.out.println(" Please enter your firstname: ");
        firstName = input.nextLine();

        System.out.println(" Please enter your lastname: ");
        lastName = input.nextLine();

        System.out.println(" Please enter your age: ");
        age = input.nextInt();
        input.nextLine();

        System.out.println(" Please enter your address: ");
        address = input.nextLine();


        System.out.println(" Please enter your email address: ");
        emailAddress = input.nextLine();

        System.out.println(" Please enter your phone number: ");
        phoneNumber = input.nextLine();

        System.out.println("\tUser who joined this program is "+firstName+ " "+lastName+". " + firstName+ "’s age is \n" +
                age +". "+ firstName+ "’s email address is " +emailAddress  + ", phone number \n "+
              " is" +phoneNumber+ " and address is" +address +".");


    }
}


