package homeworks;

import java.util.Arrays;

public class Homework18 {
    public static void main(String[] args) {
        System.out.println("==========Task1=========");
        int[] newArr = doubleOrTriple(new int[]{1, 3, 5}, true);
        System.out.println(Arrays.toString(newArr));
        int[] newArr1 = doubleOrTriple(new int[]{3, 7, 2}, false);
        System.out.println(Arrays.toString(newArr1));
        System.out.println("==========Task2=========");
        System.out.println(splitString("Java", 2));
        System.out.println(splitString("JavaScript", 5));
        System.out.println(splitString("Hello", 3));
        System.out.println("==========Task3=========");
        System.out.println(countPalindrome("Mom and  Dad"));
        System.out.println(countPalindrome("Kayak races attracts    racecar drivers"));
    }

    public static int[] doubleOrTriple(int[] arr, boolean value) {
        int[] result = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (value) result[i] = arr[i] * 2;
            else result[i] = arr[i] * 3;
        }
        return result;

    }

    public static String splitString(String str, int num) {
        if (str.length() % num != 0)
            return "";
        else
            return str.substring(0, num) + " " + str.substring(num);
    }

    public static int countPalindrome(String input) {
        String[] words = input.split("\\s+");
        int count = 0;

        for (String word : words) {
            String lowercaseWord = word.toLowerCase();
            String reversedWord = new StringBuilder(lowercaseWord).reverse().toString();

            if (lowercaseWord.equals(reversedWord)) {
                count++;
            }
        }


        return count;}

    }

