package homeworks;

import methods.CalculateAge;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class Homework11 {
    public static void main(String[] args) {
        System.out.println("=============Task1===============");
        System.out.println(noSpace(""));
        System.out.println(noSpace("Java"));
        System.out.println(noSpace("  Hello   "));
        System.out.println(noSpace("Hello World   "));
        System.out.println(noSpace("  Tech   Global   "));

        System.out.println("=============Task2===============");
        System.out.println(replaceFirstLast(" "));
        System.out.println(replaceFirstLast("A"));
        System.out.println(replaceFirstLast(" A  "));
        System.out.println(replaceFirstLast("Hello"));
        System.out.println(replaceFirstLast("Tech Global"));

        System.out.println("=============Task3===============");
        System.out.println(hasVowel(""));
        System.out.println(hasVowel("Java"));
        System.out.println(hasVowel("1234"));
        System.out.println(hasVowel("ABC"));

        System.out.println("=============Task4===============");
        checkAge(2010);
        checkAge(2006);
        checkAge(2050);
        checkAge(1920);
        checkAge(1800);

        System.out.println("=============Task5===============");
        System.out.println(averageOfEdges(0, 0, 0));
        System.out.println(averageOfEdges(0, 0, 6));
        System.out.println(averageOfEdges(-2, -2, 10));
        System.out.println(averageOfEdges(-3, 15, -3));
        System.out.println(averageOfEdges(10, 13, 20));

        System.out.println("=============Task6===============");
        String[] arr = {"java", "hello", "123", "xyz"};
        System.out.println(Arrays.toString(noA(arr)));
        String[] arr1 = {"appium", "123", "ABC", "java"};
        System.out.println(Arrays.toString(noA(arr1)));
        String[] arr2 = {"apple", "appium", "ABC", "Alex", "A"};
        System.out.println(Arrays.toString(noA(arr2)));

        System.out.println("=============Task7===============");
        int[] arr3 = {7, 4, 11, 23, 17};
        System.out.println(Arrays.toString(no3or5(arr3)));
        int[] arr4 = {3, 4, 5, 6};
        System.out.println(Arrays.toString(no3or5(arr4)));
        int[] arr5 = {10, 11, 12, 13, 14, 15};
        System.out.println(Arrays.toString(no3or5(arr5)));

        System.out.println("=============Task8===============");
        System.out.println(countPrimes(new int[]{-10, -3, 0, 1}));
        System.out.println(countPrimes(new int[]{7, 4, 11, 23, 17}));
        System.out.println(countPrimes(new int[]{41, 53, 19, 47, 67}));
    }

    public static String noSpace(String s) {

        return s.trim().replaceAll("[\\s]+", "");
    }

    public static String replaceFirstLast(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (s.length() < 2) return "";

        }
        return s.charAt(s.length() - 1) + s.substring(1, s.length() - 1) + s.charAt(0);

    }

    public static boolean hasVowel(String str) {

        for (int i = 0; i < str.length(); i++) {
            if (str.toLowerCase().contains("a") || str.toLowerCase().contains("e") || str.toLowerCase().contains("i") ||
                    str.toLowerCase().contains("u") || str.toLowerCase().contains("o")) return true;
            break;
        }

        return false;

    }

    public static void checkAge(int yearOfBirth) {
        Date date = new Date();
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int age = currentYear - yearOfBirth;
        if (age >= 16 && age < 100) System.out.println("AGE IS ALLOWED");
        else if (age < 16 && age > 0) System.out.println("AGE IS NOT ALLOWED");
        else System.out.println("AGE IS NOT VALID");


    }

    public static int averageOfEdges(int n1, int n2, int n3) {
        int max = Math.max(Math.max(n1, n2), n3);
        int min = Math.min(Math.min(n1, n2), n3);
        return (min + max) / 2;

    }

    public static String[] noA(String[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].toLowerCase().startsWith("a")) arr[i] = "###";

        }
        return arr;
    }

    public static int[] no3or5(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            if(arr[i] % 15 == 0) arr[i] = 101;
            else if(arr[i] % 3 == 0) arr[i] = 100;
            else if(arr[i] % 5 == 0) arr[i] = 99;
        }
        return arr;
    }


    public static int countPrimes(int[] numbers) {


        int prime = 0;
        boolean isNotPrime = false;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] >= 2) {
                for (int j = 2; j < numbers[i]; j++) {
                    if (numbers[i] % j == 0) {
                        isNotPrime = true;
                        break;
                    }
                }
                if (!isNotPrime) prime++;
                isNotPrime = false;
            }
        }
        return prime;
        //------------ 2ND WAY---------------------
      /*  int nonPrimeNumbers = 0;

        for (int num : numbers) {
            if(num < 2){
                nonPrimeNumbers++;
                continue;
            }
            for (int i = 2; i < num; i++) {

                if(num % i == 0) nonPrimeNumbers++;
            }
        }
        return numbers.length - nonPrimeNumbers;

       */
    }


}
