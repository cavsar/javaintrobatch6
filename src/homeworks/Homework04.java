package homeworks;

import com.sun.deploy.security.SelectableSecurityManager;
import utilities.ScannerHelper;

import java.util.Locale;

public class Homework04 {
    public static void main(String[] args) {

        System.out.println("\n=============TASK1=============\n");
        String name=ScannerHelper.getFirstName();
        int lengthOfName =name.length();
        System.out.println(lengthOfName);
        System.out.println(name.charAt(0));
        System.out.println(name.charAt(4));
        System.out.println(""+name.charAt(0)+name.charAt(1)+name.charAt(2));
        System.out.println(""+name.charAt(2)+name.charAt(3)+name.charAt(4));
        boolean startsWitha= name.startsWith("a");
        boolean startsWithA= name.startsWith("A");

        if(startsWitha||startsWithA){
            System.out.println("You are in the club!");}
        else{
            System.out.println("Sorry, you are not in the club");
        }
        if(name.charAt(0)=='a')
            System.out.println("You are in the club!");
        else
        System.out.println("Sorry, you are not in the club");
        System.out.println("\n=============TASK2=============\n");
        String myAddress =ScannerHelper.getAddress();
        if(myAddress.contains("Chicago")){
            System.out.println("You are in the club");}
        else if(myAddress.contains("Des Plaines")){

            System.out.println("You are welcome to join to the club");}
        else {
            System.out.println("Sorry, you will never be in the club");}

      System.out.println("\n=============TASK3=============\n");
        String favCountry= ScannerHelper.getCountry();
        if (favCountry.toUpperCase().contains("I") && favCountry.toLowerCase().contains("a") || favCountry.toLowerCase().contains("i")&&favCountry.toUpperCase().contains("A")) {
            System.out.println("A and i are there");}
        else if(favCountry.toUpperCase().contains("I")|| favCountry.toLowerCase().contains("i")) {
            System.out.println("I is there");}
        else if(favCountry.toUpperCase().contains("A")|| favCountry.toLowerCase().contains("a")) {
            System.out.println("A is there");}
        else {
            System.out.println("A and i are not there");}

        System.out.println("\n=============TASK4=============\n");
        String str = "   Java is FUN   ";

        String str2 =str.trim().toLowerCase();

        System.out.println("The first word in the str is = " +str2.substring(0,4));
        System.out.println("The second word in the str is = " +str2.substring(5,7));
        System.out.println("The third word in the str is = " +str2.substring(8));

    }
}
