package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework09_revised {
    public static void main(String[] args) {
        System.out.println("****TASK1****");
        System.out.println(firstDuplicatedNumber(new int[]{-4, 0, -7, 0, 5, 10, 45, 45}));
        System.out.println(firstDuplicatedNumber(new int[]{-8, 56, 7, 8, 65}));
        System.out.println(firstDuplicatedNumber(new int[]{3, 4, 3, 3, 5, 5, 6, 6, 7}));

        System.out.println("****TASK2****");
        System.out.println(firstDuplicatedString(new String[]{"Z", "abc", "z", "123", "#"}));
        System.out.println(firstDuplicatedString(new String[]{"xyz", "java", "abc"}));
        System.out.println(firstDuplicatedString(new String[]{"a", "b", "B", "XYZ", "123"}));

        System.out.println("****TASK3****");
        System.out.println(duplicatedNumbers(new int[]{0, -4, -7, 0, 5, 10, 45, -7, 0}));
        System.out.println(duplicatedNumbers(new int[]{1, 2, 5, 0, 7}));

        System.out.println("****TASK4****");
        System.out.println(duplicatedStrings(new String[]{"A", "foo", "12", "Foo", "bar", "a", "a", "java"}));
        System.out.println(duplicatedStrings(new String[]{"python", "foo", "bar", "java", "123"}));

        System.out.println("****TASK5****");
        String[] arr = {"Red", "Blue", "Yellow", "While"};
        reversedArray(arr);

        System.out.println("****TASK6****");
        System.out.println(reveredString("Java is fun"));

    }

    public static int firstDuplicatedNumber(int[] arr) {
        ArrayList<Integer> duplicates = new ArrayList<>();
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (!duplicates.contains(arr[j]) && arr[i] == arr[j]) {
                    duplicates.add(arr[i]);
                    break;
                }
            }
        }
        if (duplicates.size() == 0) {
            return -1;
        } else {
            return duplicates.get(0);
        }
    }

    public static String firstDuplicatedString(String[] arr) {
        ArrayList<String> duplicates = new ArrayList<>();
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (!duplicates.contains(arr[i].toLowerCase()) && arr[i].equalsIgnoreCase(arr[j])) {
                    duplicates.add(arr[i].toLowerCase());
                    break;
                }
            }

        }
        if (duplicates.size() == 0) {
            return "There is no duplicates";
        } else {
            return duplicates.get(0);
        }
    }

    public static ArrayList<Integer> duplicatedNumbers(int[] arr) {
        ArrayList<Integer> duplicates = new ArrayList<>();
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (!duplicates.contains(arr[j]) && arr[i] == arr[j]) {
                    duplicates.add(arr[i]);

                }
            }
        }
        if (duplicates.size() == 0) {

            return new ArrayList<>();

        } else {
            return duplicates;
        }
    }

    public static ArrayList<String> duplicatedStrings(String[] arr) {
        ArrayList<String> duplicates = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (!duplicates.contains(arr[i]) && arr[i].equalsIgnoreCase(arr[j])) {
                    duplicates.add(arr[i]);
                    break;

                }
            }
        }

        return duplicates;
    }

    public static void reversedArray(String[] arr) {
        ArrayList<String> reversedList = new ArrayList<>();
        for (int i = arr.length - 1; i > 0; i--) {
            reversedList.add(arr[i]);
        }
        System.out.println(reversedList);


}
    public static String reveredString(String str){
        String reveredStr = "";

        String[] strAsArr = str.trim().split("\\s+");
        //System.out.println(Arrays.toString(strAsArr));
        for (String s : strAsArr) {
           // System.out.println(s);
            reveredStr += new StringBuffer(s).reverse() + " ";

        }

        return reveredStr.trim();}}











