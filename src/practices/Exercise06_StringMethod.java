package practices;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise06_StringMethod {
    public static void main(String[] args) {
        String s = ScannerHelper.getString();
        int len= s.length();

        if(len<4) System.out.println("INVALID INPUT");
        else{
            if(s.startsWith("xx")&&s.endsWith("xx")) System.out.println("true");
            else System.out.println("false");
        }
    }
}
