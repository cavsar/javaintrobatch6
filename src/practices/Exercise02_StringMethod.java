package practices;

import utilities.ScannerHelper;

public class Exercise02_StringMethod {
    public static void main(String[] args) {
        String str= ScannerHelper.getString();

        int lengthOfString= str.length();
        if(lengthOfString >=3){
        if(lengthOfString %2==0) System.out.println("" + str.charAt((lengthOfString/2)-1)+str.charAt((lengthOfString/2)));
        else System.out.println(str.charAt(lengthOfString/2));}
        else System.out.println("Length is less than 3");
      //2nd way
        if(str.length() < 3) System.out.println("Length is less than 3");
        else if(str.length() % 2 == 0) System.out.println(str.substring(str.length() / 2 - 1, str.length() / 2 + 1));
        else System.out.println(str.charAt(str.length() / 2));
    }
}
