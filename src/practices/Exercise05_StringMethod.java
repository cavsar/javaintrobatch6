package practices;

import utilities.ScannerHelper;

public class Exercise05_StringMethod {
    public static void main(String[] args) {
        String s1 = ScannerHelper.getString();
        String s2 = ScannerHelper.getString();

        int len1= s1.length();
        int len2= s2.length();
        if(len1<2 || len2<2) System.out.println("INVALID INPUT");
        else System.out.println(s1.substring(1,len1-1).concat(s2.substring(1,len2-1)));
    }

}
