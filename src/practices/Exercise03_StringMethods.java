package practices;

import utilities.ScannerHelper;

public class Exercise03_StringMethods {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();
        int lengthOfString =str.length();

        if(lengthOfString<4) System.out.println("INVALID INPUT");

        else System.out.println("First 2 characters are =" + str.substring(0,2) +"\n Last 2 characters are =" +str.substring(str.length()-2)+"\nThe other characters are ="+ str.substring(2,(lengthOfString-2)));
    }
}
