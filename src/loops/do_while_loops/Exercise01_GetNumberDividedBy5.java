package loops.do_while_loops;

import utilities.ScannerHelper;

public class Exercise01_GetNumberDividedBy5 {
    public static void main(String[] args) {

        int num;
        do { num = ScannerHelper.getNumber();
            System.out.println(num);
        }
        while (num % 5 != 0);
        System.out.println("end");

        System.out.println("\n==========while loop==========\n");
        //Create an infinite loop,
    }
}
