package loops.while_loop;

public class Exercise01_PrintNumbersDividedBy3 {
    public static void main(String[] args) {
        System.out.println("\n===========for i loop===========\n");

        for (int i = 1; i <=100 ; i++) {
            if (i % 3 ==0) System.out.println(i);

        }

        System.out.println("\n===========while loop===========\n");

        int j = 1;
        while(j<=100) {
            if (j % 3 == 0) System.out.println(j);
            j++;
        }

    }
}
