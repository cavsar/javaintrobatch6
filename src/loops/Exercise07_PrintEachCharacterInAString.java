package loops;

import utilities.ScannerHelper;

public class Exercise07_PrintEachCharacterInAString {
    public static void main(String[] args) {
        String str= ScannerHelper.getString();

        int len= str.length();
        for (int i = 0; i <=len-1 ; i++) {
            System.out.println(str.charAt(i));

        }
    }
}
