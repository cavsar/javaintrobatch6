package loops.practices;

import utilities.ScannerHelper;

public class Exercise04 {
    public static void main(String[] args) {
        String s=ScannerHelper.getString().toLowerCase();

        int countvowel=0;
        for (int i = 0; i < s.length(); i++) {

            if ((s.charAt(i)=='a')|| (s.charAt(i)=='e')
                    || ( s.charAt(i)=='i')|| (s.charAt(i)=='o')||(s.charAt(i)=='u'))countvowel++;

        }
        System.out.println(countvowel);


    }
}
