package loops.practices;

import utilities.ScannerHelper;

public class Exercise02 {
    public static void main(String[] args) {
        int number;
        int counter=1;
        do{
            if(counter>1)System.out.println( "This number is not more than or equals 10");
            number= ScannerHelper.getNumber();
counter++;
    }
        while (number<10);  System.out.println( "This number is more than or equals 10");}
}
