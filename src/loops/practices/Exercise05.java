package loops.practices;

import utilities.ScannerHelper;

public class Exercise05 {
    public static void main(String[] args) {
        int fib =6;
        int num1=0, num2=1,num3;
        String answer = "";
        for (int i =1; i <=fib ; i++) {
            answer +=num1+ "-";
            num3=num1+num2;
            num1=num2;
            num2=num3;
            
        }
        System.out.println(answer.substring(0,answer.length()-3));
    }
}
