package string_Methods;

public class Exercise04 {
    public static void main(String[] args) {
        // I go to TechnoGlobal

        String str1 = "I go to TechGlobal";
        System.out.println(str1.substring(0,1));
        System.out.println(str1.substring(2,4));
        System.out.println(str1.substring(5,7));
        System.out.println(str1.substring(8));
    }
}
