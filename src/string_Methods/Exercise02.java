package string_Methods;

import utilities.ScannerHelper;

import java.util.Scanner;

public class Exercise02 {
    public static void main(String[] args) {


        String FavBook = ScannerHelper.getFavoriteBook();
        String FavQuote = ScannerHelper.getFavoriteQuote();

        System.out.println("The length of your book is =" + FavBook.length());
        System.out.println("The length of your quote is =" + FavQuote.length());
    }
}
