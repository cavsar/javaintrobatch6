package string_Methods;

public class MethodChaining {
    public static void main(String[] args) {
        String str="TechGlobal";
//single method
        System.out.println(str.toLowerCase());

        // 2 method chain
        System.out.println(str.toLowerCase().contains("tech"));
        // 3 method chain
        System.out.println(str.toUpperCase().substring(4).contains("tech"));

    }
}
